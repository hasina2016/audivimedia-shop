-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 04 Mars 2016 à 15:43
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `audividata1`
--

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name_fr` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_fr` varchar(250) NOT NULL COMMENT 'short description',
  `about_en` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `about_es` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` text,
  `description_en` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description_es` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name_fr`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=25 ;

--
-- Contenu de la table `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `name_fr`, `name_en`, `name_es`, `slug`, `about_fr`, `about_en`, `about_es`, `description_fr`, `description_en`, `description_es`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'Otto''s world ', 'El mundo sonoror de Otto', 'dc-rogan-snowboard-boots-black-rasta', 'Stimule le développement sensoriel et améliore les facultés auditives !', '', '', 'Jeu de découverte sonore et d’éducation auditive.', '', '', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 0, '2012-12-06 00:00:00', '2016-02-10 07:34:32', 1, 0, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', '', '', 'forum-aura-snowboard-boots-chocolate', 'Aide les enfants sourds dans leur maitrise de la lange Française. Histoire sous-titrée et condé en LPC. ', '', '', 'Aide à l''acquisition de la langue française. A l''attention des enseignants.', '', '', 'img_7.png', 'img_2.2.png', '302', 0, 10, NULL, 1199, 0, '2012-12-06 00:00:00', '2016-02-18 13:36:27', 1, 0, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', '', '', 'dc-lear-mittens-blue-radiance-black', '\n	\nFamiliarise efficacement les adultes débutants et les enfants au code LPC sous la forme d''un jeu de cartes. ', '', '', 'Entrainement et familiarisation au code LCP', '', '', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1139, 1, '2012-12-06 00:00:00', '2016-02-10 07:33:56', 1, 0, 0, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', '', '', 'Digivox', 'L''audiométrie vocale en toute fluidité !', '', '', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', '', '', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 5, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(22, 3, NULL, 'VideoShow', '', '', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', '', '', 'La Terrasse', 'Tests auditifs supraliminaires.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', '', '', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', '', '', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', '', '', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
