<h2>Modifier le contenue de</h2>
<h4></h4>

<div class="row">
    <div class="col-sm-5">
        <?php echo $this->Form->create('Page'); ?>
        <br />
        <?php echo $this->Form->input('parent_id', array('class' => 'form-control','label'=>'Categorie')); ?>
        <br />        
        <?php echo $this->Form->input('title', array('class' => 'form-control','label'=>'Titre')); ?>
        <br />
        <?php echo $this->Form->input('autor', array('class' => 'form-control','label'=>'Auteur')); ?>
        <br/>
        <?php echo $this->Form->input('body_fr', array('class' => 'form-control','label'=>'Français','id'=>'body_fr')); ?>
        <br/>
        <?php echo $this->Form->input('body_en', array('class' => 'form-control','label'=>'English','id'=>'body_en')); ?>
        <br/>
        <?php echo $this->Form->input('body_es', array('class' => 'form-control','label'=>'Española','id'=>'body_es')); ?>
        <br />        
        <?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>
        <br />
        <br />

    </div>
</div>
<?php $baseHref = $this->Html->url(''); ?>
<script type="text/javascript">
    CKEDITOR.replace( 'body_fr',{
        uiColor: '#8a227b',
    });
    CKEDITOR.replace( 'body_en',{
        uiColor: '#8a227b',
    } );
    CKEDITOR.replace( 'body_es',{
        uiColor: '#8a227b',
    } );
</script>