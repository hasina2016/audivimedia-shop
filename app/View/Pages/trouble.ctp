<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<div class="page-log-content-f1">    
      <div class="plane-top">
        <?php echo $this->Html->image('desing/plane_tl_'.$lng.'.png', array('alr'=>'TROUBLE ET AUDITION')) ?>
      </div>
        
      <div class="titre">
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#2D4770"></h1>
      </div>
      <div class="all-table-container">
         <div class="bloc-content top-content">
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:17px; margin-left:42px; color:#76459B"><?php echo $this->Html->image('desing/btn_rouge.png') ?> N&eacute;cessit&eacute; d&rsquo;une r&eacute;&eacute;ducation auditive</h1>
                <div class="bloc-text">
                
                </div>
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:17px; margin-left:42px; color:#76459B"><?php echo $this->Html->image('desing/btn_rouge.png') ?> Nourrir l&rsquo;oreille de l&rsquo;enfant</h1>
                <div class="bloc-text">
                
                </div>
         </div>
         <div class="bloc-content">
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:17px; margin-left:42px; color:#76459B">N&eacute;cessit&eacute; d&rsquo;une r&eacute;&eacute;ducation auditive</h1>
               
                <div class="bloc-text">
                Malgr&eacute; l&rsquo;&eacute;volution constante et tr&egrave;s positive des appareils de correction auditive de ces derni&egrave;res ann&eacute;es, nous constatons r&eacute;guli&egrave;rement que les b&eacute;n&eacute;fices qu&rsquoen tirent nos patients ne sont pas imm&eacute;diats, voire tardifs avec une forte corr&eacute;lation au degr&eacute; de surdit&eacute; et &agrave; la dur&eacute;e de privation sensorielle. Les explications &agrave; cette probl&eacute;matique sont multiples et bien connues, la principale &eacute;tant la plasticit&eacute; c&eacute;r&eacute;brale.


<br /><br />Le cortex auditif se r&eacute;organise en fonction des stimulations. La plasticit&eacute; c&eacute;r&eacute;brale, dans le cadre d&rsquo;une d&eacute;ficience auditive p&eacute;riph&eacute;rique, r&eacute;organise la tonotopie cochl&eacute;aire de sorte que les neurones originellement affect&eacute;s &agrave; des z&ocirc;nes de fr&eacute;quence non stimul&eacute;es sont r&eacute;affect&eacute;s &agrave; des fr&eacute;quences per&ccedil;ues. En d&eacute;faut de codage du message sonore, la plasticit&eacute; peut alt&eacute;rer temporairement la capacit&eacute; de r&eacute;ponse aux signaux auditifs absents. Cette d&eacute;t&eacute;rioration n&rsquo;est pas irr&eacute;versible. Une fois la perception de ces signaux restitu&eacute;e, un entra&icirc;nement auditif pourra provoquer la r&eacute;organisation de la partie du cortex concern&eacute;e. 

<br /><br />La technologie la plus avanc&eacute;e op&eacute;rant au niveau p&eacute;riph&eacute;rique, ne peut suffire &agrave; compenser les limitations dues &agrave; un syst&egrave;me auditif central peu performant. On comprend pourquoi les aides auditives les plus &eacute;volu&eacute;es ne peuvent &agrave; elles seules restaurer une audition pleinement satisfaisante et pourquoi la discrimination de la parole, qui fait appel &agrave; des comp&eacute;tences d&rsquo;analyse fine peut demeurer lacunaire.
                </div>
         
         </div>
         
         <div class="bloc-content">
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:17px; margin-left:42px; color:#76459B">Nourrir l&rsquo;oreille de l&rsquo;enfant</h1>
                
                <div class="bloc-text">
                De la m&ecirc;me fa&ccedil;on, le cortex auditif d&rsquo;un enfant n&eacute; sourd profond se voit moins sollicit&eacute; que celui d&rsquo;un enfant entendant et par cons&eacute;quent les m&eacute;canismes de reconnaissance et de discrimination se d&eacute;veloppent moins bien. Or la plasticit&eacute; du cerveau en cours de d&eacute;veloppement est remarquablement plus performante que celle du cerveau adulte. On comprend l&rsquo;importance cruciale d&rsquo;un appareillage pr&eacute;coce et d&rsquo;une stimulation auditive intensive. Il faut &ldquo;nourrir&rdquo; l&rsquo;oreille le mieux possible pour d&eacute;velopper les capacit&eacute;s auditives du cerveau.

<br /><br />Les aides auditives vont permettre aux enfants atteints de surdit&eacute; s&eacute;v&egrave;re ou profonde de s&rsquo;approprier une partie de l&rsquo;univers sonore et cela de fa&ccedil;on progressive. Cependant, les limitations des champs fr&eacute;quentiel et dynamique induisent des distorsions et r&eacute;duisent le pouvoir d&rsquo;analyse des structures fines. Les stimulations sonores quotidiennes sont donc moins identifiables spontan&eacute;ment et demandent un acte volontaire de d&eacute;codage jusqu&rsquo;&agrave; ce que les processus d&rsquo;identification et de m&eacute;morisation soient durablement fix&eacute;s corticalement. C&rsquo;est la raison pour laquelle il est important de renforcer ces processus d&rsquo;acquisition 
par des apprentissages structur&eacute;s, et ce d&rsquo;autant plus que la surdit&eacute; est importante.

<br /><br />La confrontation des exp&eacute;riences acquises par de nombreuses &eacute;quipes pluridisciplinaires travaillant avec des enfants sourds appareill&eacute;s a permis de mettre en &eacute;vidence, &ldquo; &agrave; gain proth&eacute;tique &eacute;gal &rdquo;, &agrave; quel point les performances d&rsquo;un enfant b&eacute;n&eacute;ficiant d&rsquo;une &eacute;ducation auditive m&eacute;thodique et r&eacute;guli&egrave;re pouvaient se distinguer de celles d&rsquo;un enfant ne profitant pas d&rsquo;un tel soutien.

<br /><br />S&rsquo;il est men&eacute; de mani&egrave;re ludique, le travail d&rsquo;entra&icirc;nement auditif provoque non seulement des progr&egrave;s sensibles des performances auditives mais induit de mani&egrave;re plus ou moins consciente un regard positif sur les appareils eux-m&ecirc;mes qui donnent acc&egrave;s au jeu et autorisent ces progr&egrave;s. Ainsi l&rsquo;int&eacute;r&ecirc;t de l&rsquo;enfant pour le port des appareils s&rsquo;en trouvera renforc&eacute;.

<br /><br />Si les bons r&eacute;sultats proth&eacute;tiques observ&eacute;s g&eacute;n&eacute;ralement dans les cas de surdit&eacute; moyenne favorisent l&rsquo;acceptation au point que parfois l&rsquo;enfant lui-m&ecirc;me r&eacute;clame ses appareils, dans les cas de surdit&eacute; s&eacute;v&egrave;re ou profonde, le b&eacute;n&eacute;fice procur&eacute; par les appareils n&rsquo;appara&icirc;t pas imm&eacute;diatement. Il faut du temps et du travail pour que les premiers progr&egrave;s sensibles apparaissent. Les professionnels le savent par exp&eacute;rience mais la famille, elle, n&rsquo;en a pas la preuve et doit &ecirc;tre soutenue concr&egrave;tement pour &eacute;viter le d&eacute;couragement.

<br /><br />Il est &eacute;galement essentiel de pouvoir &eacute;tablir rapidement, au cours d&rsquo;une p&eacute;riode-cl&eacute; de son d&eacute;veloppement, un bilan des comp&eacute;tences auditives de l&rsquo;enfant, de ses possibilit&eacute;s mais aussi de ses limites. Ces bilans permettront de valider l&rsquo;ad&eacute;quation des choix proth&eacute;tiques et p&eacute;dagogiques et, &eacute;ventuellement, r&eacute;orienter la strat&eacute;gie de correction de l&rsquo;audition vers d&rsquo;autres choix comme celui de l&rsquo;implant cochl&eacute;aire qui s&rsquo;impose lorsque les r&eacute;sultats obtenus avec des aides auditives conventionnelles se r&eacute;v&egrave;lent insuffisants. Une stimulation pr&eacute;coce et r&eacute;guli&egrave;re de la fonction auditive dans le cadre familial et professionnel contribue &agrave; apporter des r&eacute;ponses plus rapides &agrave; ces questions.

<br /><br />Conscients de la n&eacute;cessit&eacute; de l&rsquo;&eacute;ducation auditive, de l&rsquo;int&eacute;r&ecirc;t des technologies multim&eacute;dias dans ce contexte et de l&rsquo;absence de mat&eacute;riel &eacute;ducatif v&eacute;ritablement adapt&eacute; &agrave; la d&eacute;ficience auditive, nous avons entrepris de d&eacute;velopper des logiciels multim&eacute;dias d&eacute;di&eacute;s tant pour l&rsquo;usage professionnel que pour l&rsquo;usage familial.
                </div>
         </div>   
        </div> 
</div>