<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<div class="page-log-content-f1">    
    	<div class="plane-top">
        <?php echo $this->Html->image('desing/plane_who_'.$lng.'.png', array('alr'=>'Qui sommes nous?')) ?>
      </div>
        
      <div class="titre">
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#2D4770">Sommaire</h1>
      </div>
      <div class="all-table-container">
         <div class="bloc-content top-content">
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:17px; margin-left:42px; color:#2d4770"><?php echo $this->Html->image('desing/btn_bleu.png') ?> Naissance d&rsquo;Audivimedia...</h1>
                <div class="bloc-text">
                C'est aux balbutiements du multim&eacute;dia qu'Alain Vinet s'est investi dans l'am&eacute;lioration et l'enrichissement de la r&eacute;&eacute;ducation auditive... avec L.A.A. Multimedia
                </div>
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:17px; margin-left:42px; color:#2d4770"><?php echo $this->Html->image('desing/btn_bleu.png') ?> Aujourd&rsquo;hui</h1>
                <div class="bloc-text">
                L.A.A. Multimedia devient Audivimedia, avec plus de projets et une plus grande &eacute;quipe...
                </div>
         </div>
         <div class="bloc-content">
         		<h1 style="font-family:Arial, Helvetica, sans-serif; font-size:17px; margin-left:42px; color:#2d4770">Les d&eacute;buts</h1>
               
                <div class="bloc-text">
                C'est aux balbutiements du multim&eacute;dia qu&rsquo;Alain Vinet s&rsquo;est investi dans l&rsquo;am&eacute;lioration et l&rsquo;enrichissement de la r&eacute;&eacute;ducation auditive. Imm&eacute;diatement persuad&eacute; de l&rsquo;atout que pouvait apporter un outil bien construit dans la prise en charge des enfants d&eacute;ficients auditifs, notamment sur le plan orthophonique, il s'est entour&eacute; en 1995 d'une &eacute;quipe pluridisciplinaire pour d&eacute;velopper des biblioth&egrave;ques sonores autour de l&rsquo;int&eacute;r&ecirc;t de l&rsquo;enfant et du travail orthophonique. Progressivement, et du fait de la complexit&eacute; du travail &eacute;voluant, La Souris Bleue et la Terrasse sont des logiciels auditifs qui ont pu &ecirc;tre d&eacute;velopp&eacute;s avec le soutien d'informaticiens et d&rsquo;ing&eacute;nieurs du son pour aboutir en 1998 et 1999 &agrave; des outils informatiques qui ont aujourd'hui fait leurs preuves. 

                <br /><br />Avec de tels outils et devant la demande des parents, L.A.A. Multimedia s'est pench&eacute; sur un projet de r&eacute;&eacute;ducation auditive &agrave; poursuivre en famille et ont &eacute;t&eacute; pos&eacute;es les pr&eacute;mices du Monde Sonore d'Otto, aujourd'hui traduit en plus de 15 langues !</div>
         
         </div>
         
         <div class="bloc-content">
         		<h1 style="font-family:Arial, Helvetica, sans-serif; font-size:17px; margin-left:42px; color:#2d4770">Audivimedia</h1>
                
                <div class="bloc-text">
                En 2001, Alain Vinet cr&eacute;e le centre auditif Audivi et c'est naturellement que L.A.A. Multimedia devient Audivimedia pour 
                <br />poursuivre le d&eacute;veloppement du Monde Sonore d'Otto et amorcer d'autres projets, dont "Play-On".

                <br /><br />Parce qu'avant tout Alain Vinet est audioproth&eacute;siste p&eacute;diatrique, Audivimedia s'efforce aujourd'hui &agrave; cr&eacute;er des outils professionnels ergonomiques et efficaces pour l'activit&eacute; des audioproth&eacute;sistes et des m&eacute;decins ORL. C'est gr&acirc;ce &agrave; la participation de nos collaborateurs (Pierre Devos et Georges Ormancey, audioproth&eacute;sistes &eacute;galement) que nos outils informatiques ont pu &ecirc;tre test&eacute;s pendant plusieurs ann&eacute;es, par diff&eacute;rents professionnels et sur plusieurs sites et que nous avons pu parvenir au r&eacute;sultat dont nous sommes fiers aujourd'hui !
                </div>
         </div> 	
        </div> 
</div>