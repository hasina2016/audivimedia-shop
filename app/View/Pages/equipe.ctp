<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<div class="page-log-content-f1">
    
    	<div class="content">
        
        	<div class="plane">
            <?php echo $this->Html->image('desing/tle_equipe_'.$lng.'.png') ?>
            </div>
            
         <div class="box-1">
         
         <div class="box-head1">
         	<div class="box-head-title1">
            	Alain Vinet
            </div>
          <br /> <br /> 
          <div class="box-content">
          	<div class="content-title1">
                <?php echo $this->Html->image('desing/alain_vinet.jpg') ?>
                &nbsp;&nbsp;Audioproth&eacute;siste
            </div>
            
            <div class="content-text1">
            Exerce depuis 1980, sp&eacute;cifiquement 
            <br />aupr&egrave;s des enfants et b&eacute;b&eacute;s.
            
            <br /><br />Membre du Coll&egrave;ge National 
            <br />d'Audioproth&egrave;se.
            
            <br /><br />Membre de la Soci&eacute;t&eacute; Scientifique 
            <br />Internationale du Pr&eacute;r&eacute;glage.
            
            <br /><br />Appareillage Enfant et Adulte.
            
            <br /><br />Co-cr&eacute;ateur des logiciels "La 
            <br />terrasse", "La souris bleue", "le
            <br />monde sonore d'Otto", "Digivox" et 
            <br />"Vid&eacute;oShow".
            </div>
        
          </div>
          
          
         </div>
         
         </div>
         
                     <div class="box-2">
                     
                     <div class="box-head2">
                     		<div class="box-head-title2">
                          		Pierre Devos
                            </div>
                      <br /> <br /> 
                      <div class="box-content">
                      
                      	<div class="content-title2">
                        <?php echo $this->Html->image('desing/pierre.jpg') ?>
                &nbsp;&nbsp;Audioproth&eacute;siste 
                <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;& audiologue
                        </div>
                        
                      <div class="content-text2">
                    Exerce depuis 1999.
                    <br />Membre de la Soci&eacute;t&eacute; Scientifique 
                    <br />Internationale du Pr&eacute;r&eacute;glage.
                    
                    <br /><br />Appareillage Enfant et Adulte. 
                    
                    <br /><br />Dipl&ocirc;m&eacute; en 1999 en audiologie et 
                    audioproth&egrave;se (Bruxelles).
                      </div>
                        
                      </div> 
                          
                        </div>
                     </div>
                     
                     
                     <div class="box-3">
                     	
                        <div class="box-head3">
                        	<div class="box-head-title3">
                            	Georges Ormancey
                            </div>
                         <br /> <br />   
                       
                        <div class="box-content">
                        	
                            <div class="content-title3">
                                <?php echo $this->Html->image('desing/george.jpg') ?>
                				&nbsp;&nbsp;Audioproth&eacute;siste
                            </div>
                            
                         <div class="content-text3">
                        Exerce depuis 2013.
                        
                        <br /><br />Appareillage Enfant et Adulte.
                        <br />R&eacute;glages d'implants en h&ocirc;pitaux. 
                        
                       <br /><br /> D.U. Audiophonologie de l'Enfant 
                        <br />(Paris, 2013)
                        
                       <br /><br /> Dipl&ocirc;m&eacute; en 2013 en audioproth&egrave;se 
                       <br /> (Nancy).

                         </div>
                         
                         
                        </div>
                        
                        </div>
                     </div>
         
         
        </div>
    
    		
    </div>