<div class="content-contact-div">
	<div class="contact-left-div">
    	<div class="contact-form-container">
        	<p class="contact-form-title">
            	Formulaire de contact
            </p>
            
            <div class="left-form-contact">
            	<p class="form-contact">Nom : <input name="Nom" type="text" value="" size="25" maxlength="50" /></p>
                <p class="form-contact">Pr&eacute;nom : <input name="prenom" type="text" value="" size="25" maxlength="50" /></p>
                <p class="form-contact">Adresse : <input name="adress" type="text" value="" size="25" maxlength="50" /></p>
                <p class="form-contact">Code postale : <input name="postal-code" type="text" value="" size="25" maxlength="50" /></p>
                <p class="form-contact">E-mail : <input name="email" type="text" value="" size="25" maxlength="50" /></p>
            </div>
            
            <div class="message-form">
            	<p class="form-contact">
                	Votre message :
                </p>
                <form>
					<textarea class="message" name="nom" rows=4 cols=40>
                    </textarea>
				</form>
            </div>
            
            <div class="button-message-container">
            	<a class="no-decoration" href="#">
            	<div class="button-message">
                	Envoyer
                </div>
                </a>
                
                <a class="no-decoration" href="#">
            	<div class="button-message">
                	Effacer
                </div>
                </a>
            </div>
            
            <div style="clear:both">
            </div>
        </div>
        
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10498.533429332048!2d2.3451512!3d48.8652015!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x26986b7aa9aac11f!2sAudivimedia!5e0!3m2!1sfr!2smg!4v1454337005088" width="100%" height="510" frameborder="0" style="border:0; padding:25px 0 25px 0" allowfullscreen>
        </iframe>
        
    </div>
    
    <div class="contact-right-div">
    	<b>Audivimedia</b><br /><br />
        48 rue Montmartre 75002 Paris<br />
        T&eacute;l. : 01 44 75 84 44<br />
        Fax : 01 44 76 03 13<br />
        contact@audivimedia.fr<br /><br />
        
        M&eacute;tro : Les Halles ou Etienne Marcel<br />
        RER : Ch&acirc;telet-LesHalles <span class="little-description">(sortie place carr&eacute;e)</span><br />
        Parkings : Saint-Eustache, Forum Nord
        ou S&eacute;bastopol
    </div>
    
    <div>
        <?php echo $this->Html->image('desing/fond_singe.png') ?>
    </div>
    
    <div style="clear:both">
    </div>
</div>