<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<?php echo $this->Html->css('bootstrap') ?>
        <script type="text/javascript">
            function update(item,livr) {
                var price = document.querySelector('#price_'+item).innerHTML;
                var nbr = document.querySelector('#select_'+item).value;
                document.querySelector('#amount_'+item).innerHTML = parseInt(price,10)*parseInt(nbr,10);
                if (livr) {
                    document.querySelector('#subtotal_'+item).innerHTML = parseInt(price,10)*parseInt(nbr,10)+5;
                }else{
                    document.querySelector('#subtotal_'+item).innerHTML = parseInt(price,10)*parseInt(nbr,10);
                }
                
            }
        </script>
    <div class="container-commande">
    
    	<div class="content-commande">
        
        	<div class="head-commande">
            
            	<div class="box-left">
                    Passez votre commande
                    <h1 style="font-size:24px; font-style:italic; color:#4277BD">Choisissez vos logiciels</h1>
                </div>
                
                <div class="box-center">
                    Les livraisons sont assur&eacute;es sur la France m&eacute;tropolitaine pour 5 &euro; 
                    <br />TTC pour un colis n'&eacute;xc&eacute;dant pas 300 grammes (3 coffrets).
                    <br /><?php echo $this->Html->link('Voir les conditions générales de vente',array('controller'=>'pages','action'=>'cgv')) ?>
                </div>
                
                <div class="box-right">
                	<?php echo $this->Html->image('desing/img_commande.png') ?>
                </div>

            </div>

<!-- VIDEO SHOW -->
<?php foreach($items as $item): ?>    	
                <div class="content-video">
                	
                    <div class="img-video">
                        <?php echo $this->Html->image('/images/small/'.$item['Logiciel']['image_max']) ?>
                    </div>
                    
                    	<div class="video-show-content1">
                        	<div class="video-show-title">
                                <?php echo $this->Html->image('desing/puce_bleu_fonce.png', array('alt'=>'>')) ?>
                                 &nbsp; <?php echo $item['Logiciel']['name_'.$lng] ?>
                            </div>
                            
                            <div class="video-text1">
                                <?php echo $item['Logiciel']['about_'.$lng] ?>
                            </div>
                                                        
                        </div>
                        
                        <div class="video-show-content2">
                        	<div class="video-text2">
            <?php foreach($item['Detail'] as $detail): ?>
                <?php echo $categories[$detail['category_id']] ?><br/>
            <?php endforeach; ?>
                        	</div>
                        </div>
                        
                        <div class="qte-video">
                           	<div class="titre-qte">
                            	Quantit&eacute;<br />
                            </div>
                             <div class="select-option">
                             <?php $livr = ($item['Logiciel']['usb'] || $item['Logiciel']['cdrom'])? 1: 0; ?>
                             <input class='form-control' type = 'number' value='0' min='0' style="width:70px; height:40px; border:2px solid #4277B1; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#4277B1;" onchange="update('<?= $item['Logiciel']['id'] ?>',<?= $livr ?>)" id='<?= "select_".$item["Logiciel"]["id"]  ?>'>

                              </input>
                             </div>
      
                        </div>
                        
                        <div class="prix-video">
                        	<div class="titre-prix-video">
                            	Prix HT
                               <br /><h1 style="font-family:Arial, Helvetica, sans-serif; font-size:22px; color:#4277B1; font-weight:normal;">x <span id='<?= "price_".$item["Logiciel"]["id"]  ?>'><?php echo $item['Logiciel']['price'] ?></span> &euro;
                                <br /> = <span id='<?= "amount_".$item["Logiciel"]["id"]  ?>'>0</span> &euro;</h1>
                            </div>
 
                        </div>
                        
                        <div class="prix-ttc-video">
                        	<div class="titre-prix-ttc-video">
                            	Prix TTC
                                <br /><h2 style="font-family:Arial, Helvetica, sans-serif; font-size:22px; color:#4277B1; font-weight:normal;">&nbsp;&nbsp;<span id='<?= "subtotal_".$item["Logiciel"]["id"]  ?>'>0</span> &euro;</h2>
                            </div>
                        </div>
<?php if($item['Logiciel']['cdrom'] || $item['Logiciel']['usb'] ): ?>                        
                        <div class="checkbox-video" style="border: 0px; background-color: unset;">
                           <input type="checkbox" id="test1" class='form-control'>
                        </div>
                        <div class="text-checkbox-video">
                        	<?php echo __('Version') ?>
                            <br /><?php echo ($item['Logiciel']['cdrom'])? 'CD-Rom':'' ?>
                            <?php echo ($item['Logiciel']['usb'])? 'USB':'' ?>
                        </div>
                        <div class="liv-ht-video">
                        + 5&euro; HT + livraison
                        </div>
<?php endif; ?>
                </div>
<?php endforeach; ?>
   
            

  
                          

                
   <div class="text-etape">
    <?php echo $this->Html->link('Passer à l\'étape suivante','#') ?>
   </div>             
                  
        </div>
        
    </div>
<?php //echo var_dump($item['Detail']) ?>