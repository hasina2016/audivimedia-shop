

<h2>Modifier un produit</h2>
<h4><?php echo $logiciel['Logiciel']['name_fr'] ?></h4>


<br />

<div class="row">
    <div class="col-sm-5">

        <?php echo $this->Form->create('Logiciel'); ?>
        <?php echo $this->Form->input('id'); ?>
        <?php echo $this->Form->input('category_id', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('name_fr', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('name_en', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('name_es', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('about_fr', array('class' => 'form-control ckeditor','label'=>'A propos Français')); ?>
        <?php echo $this->Form->input('about_en', array('class' => 'form-control ckeditor','label'=>'A propos English')); ?>
        <?php echo $this->Form->input('about_es', array('class' => 'form-control ckeditor','label'=>'A propos Española')); ?>
        <?php echo $this->Form->input('description_fr', array('class' => 'form-control ckeditor','id'=>'description_fr')); ?>
        <?php echo $this->Form->input('description_en', array('class' => 'form-control ckeditor','id'=>'description_en')); ?>
        <?php echo $this->Form->input('description_es', array('class' => 'form-control ckeditor','id'=>'description_es')); ?>
        <?php echo $this->Form->input('price', array('class' => 'form-control')); ?>
        <?php echo $this->Form->input('active', array('type' => 'checkbox')); ?>
        <?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<?php $baseHref = $this->Html->url('/'); ?>
<script type="text/javascript">
    CKEDITOR.replace( 'description_fr',{
        uiColor: '#8a227b',
    } );
    CKEDITOR.replace( 'description_en',{
        uiColor: '#8a227b',
    } );
    CKEDITOR.replace( 'description_es',{
        uiColor: '#8a227b',
    } );
</script>
