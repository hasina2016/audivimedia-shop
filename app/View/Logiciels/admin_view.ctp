<script type="text/javascript">
    function Editer(urleditcnt) {
        $.ajax({
            url: urleditcnt,
            success: function(result, status){
                    $('#cnt').html(result);
                }
            });
        }
</script>

<h2><?php echo h($log['Logiciel']['name_fr']); ?></h2>
<?php echo $this->Html->Image('/images/small/' . $log['Logiciel']['image_max'], array('alt' => $log['Logiciel']['name_fr'], 'class' => 'image')); ?>
<table class="table-striped table-condensed">
    <tr>
        <td><ul>Prix</td>
        <td><ul><?php echo h($log['Logiciel']['price']); ?></td>
    </tr>
    <tr>
        <td><ul>Affiché</td>
        <td><ul><?php echo $this->Html->link($this->Html->image('icon_' . $log['Logiciel']['active'] . '.png'), array('controller' => 'logiciels', 'action' => 'switch', 'active', $log['Logiciel']['id']), array('class' => 'status', 'escape' => false)); ?></td>
    </tr>
</table>
<?php  
    $id = $log['Logiciel']['id'];
    $item = $log['Logiciel']['name_fr'];
    $urledit = $this->Html->url(array('controller'=>'logiciels','action'=>'edit',$id));
    $urlcnt = $this->Html->url(array('controller'=>'pages','action'=>'add','?'=>array('logid'=>"$id" ,'item'=> "$item")));
?>
<h3>Actions</h3>
<a href="#" id="edit" class="btn btn-default">Editer</a>
<a href="#" id="contents" class="btn btn-default" data-toggle="tooltip" title='Ajouter des contenues tel que des paragraphes ou des déscriptions'>Rediger</a>
<?php echo $this->Form->postLink('Supprimer', array('action' => 'delete', $id), array('class' => 'btn btn-danger'), __('Etes-vous sûr de vouloir supprimer # %s?', $log['Logiciel']['name_fr'])); ?>
<hr />

<h2><u>Contenus:</u></h2>
<?php foreach($log['Page'] as $page): ?>
    <?php echo '<h4>'.$page['title'].'</h4>' ?>
    <?php echo $page['body_fr'] ?>
    <?php $urleditcnt = $this->Html->url(array('controller'=>'pages','action'=>'edit', $page['id'])); ?>
    <?php $urledittoogle = $this->Html->url(array('controller'=>'pages','action'=>'edittoogle', $page['id'])); ?>
    
    <br/>
    <small>
        <a href="#" onclick="Editer('<?=  $urleditcnt ?>')">editer <i><?php echo ''.$page['title'] ?></i></a>
        <br/><span data-toggle="tooltip" title='Emplacement du text sur la présentation de la page d Accueil'>Emplacement : </span>
<?php echo $this->Html->link(''.($page['pos']== 1 ? 'gauche':'droite'),'#',array('class'=>'position','data-type'=>'select', 'data-pk'=>''.$page['id'], 'data-url'=>''.$urledittoogle, 'data-title'=>'Choisir l\'emplacement', 'id'=>'pos')); ?>        
        <br/><span data-toggle="tooltip" title="Afficher ou non\'article " >Afficher : </span>
<?php echo $this->Html->link(''.($page['active'] ? 'Oui':'Non'),'#',array('class'=>'activated','data-type'=>'select', 'data-pk'=>''.$page['id'], 'data-url'=>''.$urledittoogle, 'data-title'=>'Choisir l\'emplacement', 'id'=>'active')); ?>

    </small>

    <hr/>
<?php endforeach ?>

<hr/>

<script type="text/javascript">
    $(document).ready(function(){

        $('#edit').click(function(){
            $.ajax({
                 url: "<?= $urledit?>",
                 success: function(result,status){
                    $('#cnt').html(result);
                 }
            })
        });        

        $('#contents').click(function(){
            $.ajax({
                 url: "<?= $urlcnt?>",
                 success: function(result,status){
                    $('#cnt').html(result);
                 }
            })
        });        

        $('[data-toggle="tooltip"]').tooltip();

        $('.position').editable({
            savenochange: true,
        value: 1,    
        source: [
              {value: 1, text: 'gauche'},
              {value: 2, text: 'droite'}
           ]
    });        

        $('.activated').editable({
            mode: 'inline',
            savenochange: true,
        value: 1,    
        source: [
              {value: 1, text: 'Oui'},
              {value: 0, text: 'Non'}
           ]    
    });
    });
</script>

