
<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<?php echo $this->Html->css('logview') ?>

    <div class="container-sonore-otto">
    <?php echo $this->Flash->render('flash'); ?>
    	<div class="content-sonore-otto">
        
        	<div class="head-content-sonore">
            	<div class="img-left-sonore">
                	<?php echo $this->Html->image('/images/large/'.$logiciel['Logiciel']['image_max']) ?>
                </div>
                <br /><br />
                <div class="head-right-sonore">
                	<div class="titre-sonore-otto">
                    	<div class="titre-sonore1">
                        	<?php echo $logiciel['Logiciel']['name_'.$lng] ?>
                        </div>
                        <div class="titre-sonore2">
                        	<?php echo $logiciel['Logiciel']['description_'.$lng] ?>
                        </div>
                    </div>
                    
                    <div class="container-price-sonore">
                    	<div class="price-sonore1">
                        	<div class="price-sonore1-content">
                            	<?php echo $logiciel['Logiciel']['age'] ?><br />ans
                            </div>
                        </div>
                        
                        <div class="price-sonore2">
                        	<div class="price-sonore2-content">
                            	PC
                            </div>
                        </div>                
                        <div class="price-sonore3">
                        <?php if($logiciel['Logiciel']['try']==1): ?>
                        	<div class="price-sonore3-content">
                            	<?php echo $logiciel['Logiciel']['trydur'] ?><br />JOURS<br />D&rsquo;ESSAI<br />GRATUIT !
                            </div>
                            
                            <a class="btn-essaie" href="#">
                                    <div class="border-btn-essaie">
                                        Essayer<br />ce logiciel
                                    </div>
                            </a>
                        <?php endif ?> 
                        </div>
              
                    </div>
                    	<div class="price-sonore4">
                        	<div class="price-sonore4-content">
                            	<?php echo $logiciel['Logiciel']['price'] ?> &euro;<br />TTC
                            </div>
                            
                            <a class="btn-acheter" href="#">
                                    <div class="border-btn-acheter">
                                        <?php echo $this->Form->create(NULL, array('url' => array('controller' => 'shop', 'action' => 'add'))); ?>
                                        <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $logiciel['Logiciel']['id'])); ?>
                                        <?php echo $this->Form->input('quantity', array('type' => 'number', 'value' => 10)); ?>
                                        <?php echo $this->Form->button('Ajouter au panier', array('class' => 'btn btn-primary addtocart', 'id' => 'addtocart', 'id' => $logiciel['Logiciel']['id']));?>
                                    </div>
                            </a>
                        </div>
                        
                        <div class="price-sonore5">
                        	<div class="btn-fermer">
                            	X
                            </div>
                        </div>
                </div>
            </div>


            <div class="content-sonore-otto2">
            	<div class="content-left-sonore" style="height: inherit;">
                        <div class="content-left-sonore1">
                            <!-- CAROUCELLE -->
                            <?php echo $this->Html->image('desing/img-son_otto2.jpg') ?>
                        </div>
<?php foreach($logiciel['Page'] as $page): ?>
    <?php if($page['pos']==1): ?>   
                        <div class="accordion">
                            <div class="accordion-section">
                                <?php $href = $page['id'] ?>
                                <a class="accordion-section-title active" href="<?= '#accordion-'.$href ?>"><?php echo $page['title'] ?></a>
                                <i class="foldable__icon icon-next"></i>
                                 
                                <div id="<?= 'accordion-'.$href ?>" class="accordion-section-content">
                                    <?php echo $page['body_'.$lng] ?>
                                </div><!--end .accordion-section-content-->
                            </div><!--end .accordion-section-->
                        </div><!--end .accordion-->
    <?php endif ?>
<?php endforeach ?>   
                </div>        
                <div class="content-right-sonore" style="height: inherit;">
<?php foreach($logiciel['Page'] as $page): ?>
    <?php if($page['pos']==2): ?>
                		<div class="accordion">
                			<div class="accordion-section">
                                <?php $href = $page['id'] ?>
                                <a class="accordion-section-title" href="<?= '#accordion-'.$href ?>"><?php echo $page['title'] ?></a>
                                 
                                <div id="<?= 'accordion-'.$href ?>" class="accordion-section-content">
                                    <?php echo $page['body_'.$lng] ?>
                                </div><!--end .accordion-section-content-->
                            </div><!--end .accordion-section-->
                        </div><!--end .accordion-->
    <?php endif ?>
<?php endforeach ?>
                 </div>       
                </div>

            </div>
            
        </div>
    </div>

    
 <script type="text/javascript">
			$(document).ready(function() {
				function close_accordion_section() {
					$('.accordion .accordion-section-title').removeClass('active');
					$('.accordion .accordion-section-content').slideUp(300).removeClass('open');
				}
			 
				$('.accordion-section-title').click(function(e) {
					// Grab current anchor value
					var currentAttrValue = $(this).attr('href');
			 
					if($(e.target).is('.active')) {
						close_accordion_section();
					}else {
						close_accordion_section();
			 
						// Add active class to section title
						$(this).addClass('active');
						// Open up the hidden content panel
						$('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
					}
			 
					e.preventDefault();
				});
                $('.content-left-sonore, content-right-sonore').mCustomScrollbar({
                    theme:'3d-thick',
                    scrollButtons: {enable: true}
                });
			});
 </script>

<?php //echo var_dump($logiciel) ?>