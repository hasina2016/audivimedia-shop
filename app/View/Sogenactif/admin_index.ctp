<div class="container-fluid">
	<h3>Paramètres généraux du Module de Paiement Sogenactif</h3>
	<?php echo $this->Form->create('Settings') ?>
		<fieldset>
		<div class="row">
			<div class="col col-sm-5">
				<legend><h5>Cartes Disponibles:</h5></legend>
				<?php echo $this->Form->input('master_card', array('class'=>'form-control','type'=>'checkbox','checked'=>true)) ?>
				<?php echo $this->Form->input('visa', array('class'=>'form-control','type'=>'checkbox','checked'=>true)) ?>
				<?php echo $this->Form->input('cb', array('class'=>'form-control','type'=>'checkbox','checked'=>true)) ?>
				<?php echo $this->Form->input('pay_lib', array('class'=>'form-control','type'=>'checkbox','checked'=>true)) ?>
			</div>
			<div class="col col-sm-3">
				<legend><h5>Ordre d'Affichage:</h5></legend>
				<ul id="sortable">
					  <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Master Card</li>
					  <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Visa</li>
					  <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>CB</li>
					  <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Pay Lib</li>
				</ul>
			</div>
		</div>	
		<div class="row">
			<div class="col col-sm-8">
				<legend><h5>Messages:</h5></legend>
				<?php echo $this->Form->input('success',array('class'=>'form-control input-sm','type'=>'text', 'label'=>'Message en cas de succès:', 'value'=>''.Configure::read('Sogenactif.SUCCESS_MSG'))) ?>
				<?php echo $this->Form->input('fail',array('class'=>'form-control input-sm','type'=>'text', 'label'=>'Message en cas de failure ou annulation:','value'=>''.Configure::read('Sogenactif.ERROR_MSG'))) ?>
			</div>
		</div>
		<div class="row">	
				<legend><h5>Monetaire</h5></legend>
				<div class='col col-sm-1'><?php echo $this->Form->input('devise',array('class'=>'form-control input-sm','type'=>'text', 'value'=>'Euro')) ?></div>
				<div class='col col-sm-1'><?php echo $this->Form->input('tva',array('class'=>'form-control input-sm','type'=>'numeric','placeholder'=>'0.2 pour 20%')) ?></div>
		</div>			
		<div class="row">	
				<legend><h5>Avancé | Marchant</h5></legend>
				<div class='col col-sm-3'><?php echo $this->Form->input('id',array('class'=>'form-control input-sm','type'=>'text', 'placeholder'=>''.Configure::read('Sogenactif.ID'))) ?></div>
				<div class='col col-sm-2'><?php echo $this->Form->input('pays',array('class'=>'form-control input-sm','type'=>'numeric','value'=>''.Configure::read('Sogenactif.COUNTRY'))) ?></div>
		</div>	
		</fieldset>
		<br/>	
		<div class="row">
			<div class="col col-sm-8" align="right"><a href="#" class='btn btn-primary btn-xs'>Sauvegarder</a></div>	
		</div>
		</div>
	<?php echo $this->Form->end() ?>	
	<hr/>
</div>