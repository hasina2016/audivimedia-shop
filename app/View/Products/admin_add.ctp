<h2>Ajouter un Produit</h2>

<br />

<div class="row">
    <div class="col-sm-5">

        <?php echo $this->Form->create('Product',array('type' => 'file')); ?>
        <br />
        <fieldset>
            <legend>Categories</legend>
            <?php foreach($categories as $categorie): ?>
                <?php echo $this->Form->input($categorie['Category']['name'],array('type'=>'checkbox')) ?>
            <?php endforeach; ?>
        </fieldset>
        <?php echo $this->Form->input('image', array('type'=>'file','onchange'=>'PreviewImage();','id'=>'uploadImage')); ?>
        <br />
        <img id="uploadPreview" class="form-control" style="width: 100px; height: 100px;" />
        <br/>
        <?php echo $this->Form->input('name', array('class' => 'form-control')); ?>
        <br/>
        <?php echo $this->Form->input('description', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('price', array('class' => 'form-control')); ?>
        <br />
        <?php echo $this->Form->input('active', array('type' => 'checkbox')); ?>
        <br />
        <?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>
        <br />
        <br />

    </div>
</div>
<script type="text/javascript">
    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>