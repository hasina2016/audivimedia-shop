<!-- PAGE FAMILLE -->
<div class="page-log-content-f1">
	<div class="plane-top">
        <?php echo $this->Html->image('desing/tle_log_adapt.png'); ?>
    </div>
    
    <div class="all-table-container">
    <?php foreach ($products as $product):?>
    <?php foreach ($product['Detail'] as $detail):?>
        <?php if($detail['category_id'] == 2): ?>
    <table class="log-list">
		<tr>
        	<th class="img-table-content" rowspan="4">
            	<?php echo $this->Html->image('/images/small/' . $product['Product']['image_max']); ?> <!-- MINIATURE -->
            </th>
            <th colspan="2">
            	<div class="log-list-title">
            	<?php echo $product['Product']['name']; ?> <!-- TITRE -->
                </div>
            </th>
            <th rowspan="3">
            	<div class="price-table">
                	<div class="price-left">
                    	<div class="price-left-content">
                    	2+<br />ans <!-- olana -->
                        </div>
                    </div>
                    
                    <div class="price-center">
                    	<div class="price-center-content">
                    	<?php echo $product['Product']['price']; ?> &#8364;<br />TTC
                        </div>
                    </div>
                    
                    <div class="price-right">
                    	<div class="price-right-content">
                    	PC
                        </div>
                    </div>
                </div>
                <a class="btn-buy" href="#">
                <div class="bordered-btn-buy">
                	Acheter <!-- olana -->
                </div>
                </a>
            </th>
        </tr>
        <tr>
            <td rowspan="3">
            	<div class="vertical-line-purple-b-t">
                </div>
            </td>
            <td>
            	<div class="table-txt-content1" style="max-width:290px">
                    <?php echo $product['Product']['description']; ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            	<div class="table-txt-button">
<?php echo $this->Html->link($this->Html->image('desing/btn_rg.png'). ' En savoir plus', array('controller' => 'products', 'action' => 'view',$product['Product']['id']), array('escapeTitle' => false, 'title' => 'En Savoir plus')); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            	<div class="table-txt-content2">
            		<?php echo $product['Product']['description']; ?>
                </div>
            </td>
        </tr>
	</table>
<?php endif; ?>
   <?php endforeach; ?>
   <?php endforeach; ?>
    </div>
        <div class="img-foot">
            <?php echo $this->Html->image('desing/img-ft_2.png'); ?>
        </div>
    
    <div style="clear:both">
    </div>
</div>
