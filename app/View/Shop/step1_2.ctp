
<style type="text/css">
<!--
.Style2 {color: #4277BD; }
-->
</style>
    	<div class="content-commande3">
        	<div class="head-commande3">
            	<div class="text-head-commande3">
                	Passez votre commande - &eacute;tape 3
                </div>
                <div class="text-head-commande4">
                	Choisissez votre moyen de paiement
                </div>
            </div>
            
            <div class="virement-etape3">
            	<div class="titre-virement-etape3">
                	<?php echo $this->Html->image('desing/puce-blue.png') ?>
                        Par ch&egrave;que ou par virement
              </div>
                <div class="texte-virement">
                	<p><br />
               	      Vous allez recevoir un Email de demande de confirmation.
               	      <br />
               	      A sa r&eacute;ception, cliquez sur le lien propos&eacute; pour confirmer votre commande. <br />
           	          <br />
               	      Vous devrez ensuite adresser votre r&egrave;glement : <br />
               	      </p>
                	<blockquote>
                	  <p>Soit par ch&egrave;que &agrave; l&rsquo;ordre de : <br />
                	    Audivim&eacute;dia, 48 rue Montmartre, 75002 Paris <br />
               	        <br />
                	    Soit par virement (les coordonn&eacute;es bancaires vous seront transmises dans <br />
                	    l&rsquo;Email de confirmation de votre commande). </p>
              	  </blockquote>
                </div>
                
  				<div class="content-virement2">
                	<div class="bouton-envoyer">
                		<img src="http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/img/loading.gif" id='loading' style="display: none;" />
                    	<a href="#loading" onclick="update()" style="text-decoration:none; color:#ffffff;">Evoyer</a>
                	</div>
                	<div class="text-virement-right">
                		 <br />&Acirc; compter de la reception de votre commande, vous disposez
                    	<br />d&rsquo;un d&eacute;lai de r&eacute;tractation de 10 jours avant l&rsquo;utilisation de vos produits
               		</div>
                </div>

            </div>
            
            <div class="carte-bancaire">
                 	<div class="titre-bancaire">
                        <?php echo $this->Html->image('desing/puce-blue.png') ?>
                         Par carte bancaire
                    </div>
                    <div class="text-bancaire">
                        <br />Vous allez &ecirc;tre dirig&eacute; vers la pae de paiement en ligne
                        <br /><br />Suivez les instructions et valider.
                        <br /><br />Vous recevrez alors un mail r&eacute;sumant votre commande.
                    </div>
                    
                    <div class="type-carte">
                    	<div class="titre-type-carte">
                        	Choisissez votre type de carte :
                        </div>
                        <div class="text-type-carte">
                        	Paiement s&eacute;curis&eacute; gr&acirc;ce au formulaire standard s&eacute;curis&eacute; SSL
                        </div>
                        <div class="bloc-carte">
                        	<div class="cb">
                            	<a href="#"><?php echo $this->Html->image('http://www.cartes-bancaires.com/IMG/gif/approbalCBactu.gif') ?></a>
                            </div>
                            <div class="visa">
                            	<a href="#"><?php echo $this->Html->image('https://www.visa.fr/media/images/visalogo12-291.png') ?></a>
                            </div>
                            <div class="mastercard">
                            	<a href="#"><?php echo $this->Html->image('http://www.mastercard.com/fr/particuliers/_globalAssets/img/nav/navl_logo_mastercardcom.png') ?></a>
                            </div>
                            <div class="pay">
                            	<a href="#"><?php echo $this->Html->image('http://www.paylib.fr/img/logo.png') ?></a>
                            </div>
                        </div>
                        
                    </div>
         
            </div>

           
    </div>
<script type="text/javascript">
  function update(url = '/') {
     document.querySelector('.bouton-envoyer').style.backgroundColor = 'white';
     document.querySelector('.bouton-envoyer a').innerHTML = 'Envoie du mail...';
     document.querySelector('.bouton-envoyer a').style.color = '#4277BD';
     document.querySelector('#loading').style.display = 'inline';

     $.ajax({
        url: url,
        dataType: 'html',
        success: function(data, status, xjs){
          	//send email to $loged['email'];
        }
     });

  }

</script>