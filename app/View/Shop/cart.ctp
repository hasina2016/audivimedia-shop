<div class="page-log-content-f1">
<?php echo $this->Flash->render('flash'); ?>
<?php echo $this->set('title_for_layout', __('Panier')); ?>
<?php $this->Html->addCrumb('Shopping Cart'); ?>
<?php echo $this->Html->script(array('cart.js'), array('inline' => false)); ?>
<?php echo $this->Html->css('bootstrap') ?>
<h1>Panier</h1>
<?php if(empty($shop['OrderItem'])) : ?>
    <?php echo __('Le pannier est vide') ?>
<?php else: ?>

<?php echo $this->Form->create(NULL, array('url' => array('controller' => 'shop', 'action' => 'cartupdate'))); ?>

<hr>

<div class="row">
    <div class="col col-sm-1">#</div>
    <div class="col col-sm-7">ITEM</div>
    <div class="col col-sm-1">PRIX</div>
    <div class="col col-sm-1">QUANTITE</div>
    <div class="col col-sm-1">SOUS TOTAL</div>
    <div class="col col-sm-1">ACTION</div>
</div>

<?php $tabindex = 1; ?>
<?php foreach ($shop['OrderItem'] as $key => $item): ?>

    <div class="row" id="row-<?php echo $key; ?>">
        <div class="col col-sm-1"><?php echo $this->Html->image('/images/xsmall/' . $item['Logiciel']['image_max'], array('class' => 'px60')); ?></div>
        <div class="col col-sm-7">
            <strong><?php echo $item['Logiciel']['name_fr']; ?></strong>
            <?php
            $mods = 0;
            if(isset($item['Logiciel']['productmod_name'])) :
            $mods = $item['Logiciel']['productmod_id'];
            ?>
            <br />
            <small><?php echo $item['Logiciel']['productmod_name']; ?></small>
            <?php endif; ?>
        </div>
        <div class="col col-sm-1" id="price-<?php echo $key; ?>"><?php echo $item['Logiciel']['price']; ?></div>
        <div class="col col-sm-1"><?php echo $this->Form->input('quantity-' . $key, array('div' => false, 'class' => 'numeric form-control input-small', 'label' => false, 'size' => 2, 'maxlength' => 2, 'tabindex' => $tabindex++, 'data-id' => $item['Logiciel']['id'], 'data-mods' => $mods, 'value' => $item['quantity'])); ?></div>
        <div class="col col-sm-1" id="subtotal_<?php echo $key; ?>"><?php echo $item['subtotal']; ?></div>
        <div class="col col-sm-1">
            <span class="remove" id="<?= $key; ?>"><?php echo $this->Html->link('Supprimer', array('controller'=>'shop', 'action'=>'remove',$item['Logiciel']['id']), array('class'=>'btn btn-danger btn-xs')) ?></span>
        </div>
    </div>
<?php endforeach; ?>

<hr>

<div class="row">
    <div class="col col-sm-12">
        <div class="pull-right">
        <?php echo $this->Html->link('<i class="icon-remove icon"></i> Vider le panier', array('controller' => 'shop', 'action' => 'clear'), array('class' => 'btn btn-danger', 'escape' => false)); ?>
        &nbsp; &nbsp;
        <?php echo $this->Form->button('<i class="icon-refresh icon"></i> Metre à jour', array('class' => 'btn btn-default', 'escape' => false));?>
        <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col col-sm-12 pull-right tr">
        Sous Total: <span class="normal" id="subtotal">&euro; <?php echo $shop['Order']['subtotal']; ?></span>
        <br />
        <br />

        <?php //echo $this->Html->link('<i class="glyphicon glyphicon-arrow-right"></i> Checkout', array('controller' => 'shop', 'action' => 'address'), array('class' => 'btn btn-primary', 'escape' => false)); ?>

        <br />
        <br />

<!--         
    <?php echo $this->Form->create(NULL, array('url' => array('controller' => 'shop', 'action' => 'step1'))); ?>
            <input type='image' name='submit' src='https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif' border='0' align='top' alt='Check out with PayPal' class="sbumit" />
    <?php echo $this->Form->end(); ?> 
-->
    <?php echo $this->Html->link('Poursuivre',array('controller'=>'shop','action'=>'step1')) ?>
    </div>
</div>

<br />
<br />

<?php endif; ?>
</div>