<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title_for_layout; ?></title>
    <?php 
        echo $this->Html->css(array(
            '/admin_menu/bootstrap/css/bootstrap.min.css',
            '/admin_menu/css/waves.min.css',
            '/admin_menu/css/style.css',
            '/admin_menu/css/menu-light.css',
            '/admin_menu/font-awesome/css/font-awesome.min.css',
            'admin.css'
            )); 
    ?>
    <?php echo $this->Html->css('acceuil_admin') ?>
        <!-- scripts -->
    <?php 
        echo $this->Html->script(array(
            'http://code.jquery.com/jquery-2.0.3.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/i18n/jquery-ui-i18n.js',
            '//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js',
            '/admin_menu/js/metisMenu.min.js',
            '/admin_menu/js/jquery-jvectormap-1.2.2.min.js',
            '/admin_menu/js/flot/jquery.flot.js',
            '/admin_menu/js/flot/jquery.flot.tooltip.min.js',
            '/admin_menu/js/flot/jquery.flot.resize.js',
            '/admin_menu/js/flot/jquery.flot.pie.js',
            '/admin_menu/js/chartjs/Chart.min.js',
            '/admin_menu/js/pace.min.js',
            '/admin_menu/js/waves.min.js',
            '/admin_menu/js/morris_chart/raphael-2.1.0.min.js',
            '/admin_menu/js/morris_chart/morris.js',
            '/admin_menu/js/morris_chart/morris.js',
            '/admin_menu/js/jquery-jvectormap-world-mill-en.js',
            '/admin_menu/js/custom.js',
            '/admin_menu/js/chartjs/Chart.min.js'
            )) ;
    ?>
    <?php echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css') ?>
    <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js') ?>
    <?php echo $this->Html->script('/ckeditor/ckeditor.js') ?>
    <?php echo $this->Html->script('/ckeditor/js/sample.js') ?>
    <?php echo $this->Html->css('/ckeditor/css/samples.css') ?>
    <?php echo $this->Html->css('/ckeditor/toolbarconfigurator/lib/codemirror/neo.css') ?>
    <style type="text/css">
        #cnt{
            margin-left: 350px;
        }
    </style>
</head>
<body>
    <section class = 'page'>
        <?php echo $this->element('admin_menu'); ?>
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
    </section>
    
    <?php echo $this->Js->writeBuffer(); ?>
</body>
</html>

