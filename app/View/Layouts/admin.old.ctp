<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title><?php echo $title_for_layout; ?></title>
    <?php echo $this->Html->css(array('bootstrap.min.css', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css', 'admin.css')); ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

    <?php echo $this->Html->script(array('bootstrap.min.js', 'admin.js')); ?>

    <?php echo $this->App->js(); ?>

    <?php echo $this->fetch('css'); ?>
    <?php echo $this->fetch('script'); ?>
</head>
<body>

    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle='dropdown'>Pages<b class="caret"></b></a>
                    <ul class='dropdown-menu'>
                        <li><?php echo $this->Html->link('Acceuil', '#'); ?></li>     
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Utilisateur<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><?php echo $this->Html->link('Consulter', array('controller' => 'users', 'action' => 'index', 'admin' => true)); ?></li>
                        <li><?php echo $this->Html->link('Ajouter', array('controller' => 'users', 'action' => 'add', 'admin' => true)); ?></li>
                        <li><?php //echo $this->Html->link('Products CSV Export', array('controller' => 'products', 'action' => 'csv', 'admin' => true)); ?></li>
                    </ul>
                </li>     
                <li><?php echo $this->Html->link('Commandes', '#'); ?></li> 
                <li><?php echo $this->Html->link('Logiciels', array('controller' => 'logiciels', 'action' => 'index', 'admin' => true)); ?></li>          
                <li><?php echo $this->Html->link('Produits', array('controller' => 'products', 'action' => 'index', 'admin' => true)); ?></li>          
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Paramètres<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><?php echo $this->Html->link('Paramètres', '#'); ?></li>
                        <li><?php echo $this->Html->link('Trie', array('controller' => 'orders', 'action' => 'index', 'admin' => true)); ?></li>
                        <li><?php echo $this->Html->link('Trier sur', array('controller' => 'order_items', 'action' => 'index', 'admin' => true)); ?></li>
                        <li><?php echo $this->Html->link('Categories', array('controller' => 'categories', 'action' => 'index', 'admin' => true)); ?></li>
                        <li><?php echo $this->Html->link('Media', array('controller' => 'Media', 'action' => 'upload', 'admin' => true)); ?></li>
                        <li><?php echo $this->Html->link('Contenues', array('controller'=>'pages','action'=>'index')); ?></li> 
                    </ul>
                </li>
                
            </ul>
        </div>
    </div>

    <div class="content">
        <div class="row">   
            <small>
                <?php echo $this->Html->link('Acceuil', '/'); ?>
                <?php echo $this->Html->link('Se déconneter', array('controller' => 'users', 'action' => 'logout', 'admin' => false)); ?>
            </small>
        </div>
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
    </div>

    <div class="footer">
        <p>&copy; <?php echo date('Y'); ?> <?php echo env('HTTP_HOST'); ?></p>
    </div>

 <!--    <div class="sqldump">
        <?php //echo $this->element('sql_dump'); ?>
    </div> -->

</body>
</html>

