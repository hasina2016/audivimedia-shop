
<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $title_for_layout; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php //echo $this->Html->css('bootstrap.min.css'); ?>
    <?php echo $this->Html->css('style.css'); ?>
    <?php echo $this->Html->css('pages.css'); ?>
    <?php echo $this->Html->css('commandes.css'); ?>
    <?php echo $this->Html->css('jquery.mCustomScrollbar'); ?>
    <?php echo $this->Html->css("/_popup/css/style"); ?>
    <?php echo $this->Html->script("/_popup/js/modernizr.custom"); ?>
    <?php echo $this->Html->script('http://code.jquery.com/jquery-1.11.1.min.js'); ?>
    <?php echo $this->Html->script('jquery.mCustomScrollbar.concat.min'); ?>

</head>
<body>
<!--header-->
    <div class ='header'>
        <div class="logo-div">
            <?php echo $this->Html->link($this->Html->image('desing/logoAudivimedia_'.$lng.'.png', array('class'=>'logo')),array('controller'=>'logiciels', 'action'=>'index'), array('escapeTitle' => false, 'title' => 'Audivimedia')); ?>
        </div>
            
        <div class="flag">
            <?php echo $this->I18n->flagSwitcher(array(
                            'class' => 'languages',
                            'id' => 'language-switcher'
                        )); ?>
            <style type="text/css">
                .languages li{
                    list-style: none;
                    display: inline-block;
                    margin-left: 3px;
                }
            </style>
        </div>
<!-- loging form -->
        <?php if ($this->Session->check('Auth.User')) { ?>
            <div class="form-cmpt">
                <?php echo $this->Html->link(__('Deconnection'), array('controller'=>'Users','action'=>'logout'));?>
            </div>
            <div class="form">
                <?php
                    if ($this->loged->isAdmin()) {
                        echo $this->Html->link(__('Configuration'),'/'.$this->Session->read('Auth.User.role'));
                    }else{
                        echo $this->Html->link(__('Mes Commandes'),array('controller'=>'shop','action'=>'cart'));  
                    }
                ?>
            </div>            
            <div class="form">
                <a href="#"><?php echo __('Mes logiciels') ?></a>
            </div>            
            <div class="form">
                <a href="#"><?php echo __('Mon compte') ?></a>
            </div>
        <?php
        }else{
            echo $this->element('element_login');
        }
            
        ?>

        <div style="clear:both"></div>
    </div>
<!-- navigation menu --> 
    <div class ='nav'>
        <nav id="primary_nav_wrap">
            <ul>
                <li class="red-menu"><?php echo $this->Html->link(__('ACCUEIL'),array('controller'=>'logiciels', 'action'=>'index')); ?></li>
                <li class="purple-menu"><?php echo $this->Html->link(__('LOGICIELS FAMILLE'), array('controller' => 'logiciels', 'action' => 'famille')); ?></li>
                <li class="dark-blue-menu"><a href="#"><?php echo __('LOGICIELS PROFESSIONNELS') ?></a>
                    <ul>
                        <li><?php echo $this->Html->link(__('AUDIOPROTHESISTES'), array('controller' => 'logiciels', 'action' => 'audioprothesistes')); ?></li>
                        <li><?php echo $this->Html->link(__('ORL & PHONIATRE'), array('controller' => 'logiciels', 'action' => 'orlphoniatres')); ?></li>
                        <li><?php echo $this->Html->link(__('ORTHOPHONISTES'), array('controller' => 'logiciels', 'action' => 'orthophonistes')); ?></li>
                        <li><?php echo $this->Html->link(__('LOGICIELS ADAPTÉS À VOTRE MÉTIER'), array('controller' => 'logiciels', 'action' => 'bycategory')); ?></li>
                    </ul>
                </li>
                <li class="orange-menu"><?php echo $this->Html->link(__('COMMANDES'), array('controller' => 'orders', 'action' => 'index')); ?></li>
                <li class="blue-menu"><a href="#"><?php echo __('EN SAVOIR PLUS'); ?></a>
                    <ul>
                        <li><?php echo $this->Html->link(__('QUI SOMMES NOUS'), array('controller'=>'pages','action'=>'read','15')); ?></li>
                        <li><?php echo $this->Html->link(__('L\'EQUIPE'), array('controller'=>'pages','action'=>'equipe')); ?></li>
                        <li><?php echo $this->Html->link(__('TROUBLES ET AUDITION'), array('controller'=>'pages','action'=>'trouble')); ?></li>
                    </ul>
                </li>
                <li class="green-menu"><?php echo $this->Html->link(__('CONTACT'), array('controller' => 'pages', 'action' => 'contacte')); ?></li>
            </ul>
        </nav>
    </div>
<!-- the Content -->
<?php echo $content_for_layout; ?>
<?php //echo $this->element('element_prompted'); ?>

<!-- Popup -->
<div class="modal blur-effect" id="popup">
    <div class="popup-content">
        <div>
            <div id="cnt"></div>
            <div class="close"></div>
        </div>
    </div>
</div>
<div class="overlay"></div>
<script>
    // this is impor        tant for IEs
    var polyfilter_scriptpath = '/js/';
</script>

<!-- footer -->
    <?php echo $this->element('element_footer'); ?>
    <?php echo $this->Js->writeBuffer(); ?>
    <?php //echo $this->Html->script('/_popup/js/popup.js') ?>

    <?php //echo $this->Html->script("/_popup/js/cssParser"); ?>
    <?php //echo $this->Html->script("/_popup/js/css-filters-polyfill"); ?>
</body>
</html>

