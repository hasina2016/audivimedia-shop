<script type="text/javascript">
    function load(url){
        $('img#loading').show();
        $.ajax({
            url: url,
            success: function(result, status){
                    $('#cnt').html(result);
                    $('img#loading').hide('slow');
                    return status;
                }
        });
    }
</script>
<div id="connexion">
    <?php echo $this->Html->link('Se deconneter', array('controller' => 'users', 'action' => 'logout', 'admin' => false),array('id'=>'linkDeconnect')); ?>
    <?php echo $this->Html->link($this->Html->image('desing/home.png',array('id'=>'home')),'/admin', array('escapeTitle' => false, 'title' => 'Aceuil')); ?>
    <img src="http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/img/loading.gif" id='loading' style="display: none;" />
</div>
<div style="clear: both;"></div>
<div id="cnt">
    <div id="adminPage">
        <div id="elementPage">
            <?php $url = $this->Html->url(array('controller'=>'pages','action'=>'index')) ?>
            <a href="#" onclick="load('<?= $url ?>')">
                <?php echo $this->Html->image('desing/1.png') ?>
                <span id='titlePage'>Pages</span>
            </a>
        </div>
        <div id="elementUtilisateur">
            <?php $url = $this->Html->url(array('controller'=>'users','action'=>'index')) ?>
            <a href="#" onclick="load('<?= $url ?>')">
                <?php echo $this->Html->image('desing/2.png') ?>
                <span id='titleUtilisateur'>Utilisateurs</span>
            </a>
        </div>
        <div id="elementCommande">
            <?php $url = $this->Html->url(array('controller'=>'logiciels','action'=>'index')) ?>
            <a href="#" onclick="load('<?= $url ?>')">
                <?php echo $this->Html->image('desing/3.png') ?>
                <span id='titleCommande'>Commandes</span>
            </a>
        </div>
        <div id="elementLogiciel">
            <?php $url = $this->Html->url(array('controller'=>'logiciels','action'=>'index')) ?>
            <a href="#" onclick="load('<?= $url ?>')">
                <?php echo $this->Html->image('desing/4.png') ?>
                <span id='titleLogiciel'>Logiciels</span>
            </a>
        </div>
        <div id="elementProduit">
            <?php $url = $this->Html->url(array('controller'=>'products','action'=>'index')) ?>
            <a href="#" onclick="load('<?= $url ?>')">
                <?php echo $this->Html->image('desing/5.png') ?>
                <span id='titleProduit'>Produits</span>            
            </a>
        </div>
        <div id="elementParametre">
            <?php $url = $this->Html->url('#') ?>
            <a href="#" onclick="load('<?= $url ?>')">
                <?php echo $this->Html->image('desing/6.png') ?>
                <span id='titleParametre'>Paramètres</span>
            </a>
        </div>
    </div>
</div>