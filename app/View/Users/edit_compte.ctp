<div class="content-inscrire">
        <div class="titre-inscrire">
           Votre Compte
            <div class="sep-inscrire">
            </div>
            </div>
        <?php echo $this->Form->create('User');?>
       
        <fieldset >
            <legend><?php echo __('Votre Profil');?></legend>
            <?php echo $this->Form->input('User.username', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.lastname', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.cabinet', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.profession', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.email', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.rue', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.ville', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.tel', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.cp', array('label' => "", 'class' => 'um-form-field valid'));?></br>
            <?php echo $this->Form->input('User.pays', array('label' => "", 'class' => 'um-form-field valid'));?></br>
        </fieldset>
        <?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary')); ?>
       
        <?php echo $this->Form->end(); ?>
    </div>
</div>