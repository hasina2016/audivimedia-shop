<!-- prompted permet de définir qu'un produit ou logiciel est présenté sur la page d'accueil -->

<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<div class="home-box-container">   
        <div class="home-box1">
            <div class="home-box-head1">
                <div class="box-h-big-title"><?php echo __('Espace Familles') ?></a></div>
                <div class="box-h-title"><span class='chng' id='leftdesc' data-original-title = 'Modifer le contenue'><?php echo $xml->getElementsByTagName('left_'.$lng)->item(0)->nodeValue ?></span></div>
            </div>
            <div class="h-box-content1"> 
                <div class="box-left-content">
                    <!-- prompted logiciels -->
                    <?php $i = 0 ?>
<?php foreach($familles as $logiciel): ?>
    <?php $i++; ?>
                    <div>
                        <?php //echo $this->Html->image('desing/img_8.png'); ?> <!-- miniature -->
                        <?php echo $this->Html->image('/images/xsmall/' . $logiciel['Logiciel']['image_max'],array('id'=>'image_'.$logiciel['Logiciel']['id'])); ?>
                        <div class="red-title">
<?php $url_edittoogle = $this->Html->url(array('controller'=>'logiciels','action'=>'edittoogle')); ?>
<?php echo $this->Html->link($logiciel['Logiciel']['name_'.$lng], array('controller' => 'logiciels', 'action' => 'view',$logiciel['Logiciel']['id']),array('class'=>'log','data-url'=>''.$url_edittoogle,'data-pk'=>''.$logiciel['Logiciel']['id'], 'data-org'=>'family_'.$i)); ?>
                        </div> <!-- nom du produits -->
                        <?php $desc_id = 'desc_'.$logiciel['Logiciel']['id'] ?>
                        <div class="blue-description" id = "<?= $desc_id ?>"><?php echo $logiciel['Logiciel']['about_'.$lng]; ?></div> <!-- description -->
                    </div>
<?php endforeach ?>
                </div>
                <div class="box-right-content">
                    <div class="vert-purple-bar"></div>
                    <div class="purple-text"><span class="chng" id='purpledesc' data-original-title = 'Modifer le contenue'><?php echo $xml->getElementsByTagName('left_more_'.$lng)->item(0)->nodeValue ?></span></div>
                    <div style="clear:both"></div>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="h-box-foot1"></div>
        </div>
        <div class="home-box2">
            <div class="home-box-head2">
                <div class="box-h-big-title"><?php echo __('Espace Professionnels') ?></div>
                <div class="box-h-title"><span class='chng' id='rigthdesc' data-original-title = 'Modifer le contenue'><?php echo $xml->getElementsByTagName('rigth_'.$lng)->item(0)->nodeValue ?></span></div>
            </div>
           
            <div class="h-box-content2">
                <div class="content-box2-left">
<?php foreach($categories as $category): ?>
            <?php echo $this->Html->image('desing/puce_vert.png').'  '.$category['Category']['name'] ?> <br/>
<?php endforeach ?>
                </div>
                
                <div class="content-box2-right">
                    <?php echo $this->Html->image('desing/casque.png'); ?>
                </div>
                
                <div style="clear:both"></div>
                
                <div class="img-list-content">
                <?php $i = 0 ?>
<?php foreach($pros as $logiciel): ?>
    <?php $i++; ?>       
                    <div class="img-list">
                        <?php echo $this->Html->image('/images/xsmall/'.$logiciel['Logiciel']['image_max'],array('class'=>'log_pros','data-org'=>'pro_'.$i,'data-url'=>''.$url_edittoogle,'data-pk'=>''.$logiciel['Logiciel']['id'],'id'=>'image_pro_'.$logiciel['Logiciel']['id'])); ?>
                    </div>
    
<?php endforeach ?>                
                    <div style="clear:both"></div>
                </div>
                
                <div class="content-box2-bottom-txt">
                    <?php echo $this->Html->image('desing/puce_bleu.png'); ?> <?php echo __('Vos nouveaux outils !') ?>
                </div>
                
                <div class="img-list-content">
                <?php $i = 0 ?>
<?php foreach($news as $logiciel): ?>
    <?php $i++; ?>
                    <div class="img-list">
                        <?php echo $this->Html->image('/images/xsmall/'.$logiciel['Logiciel']['image_max'], array('class'=>'log_news','data-org'=>'new_'.$i,'data-url'=>''.$url_edittoogle,'data-pk'=>''.$logiciel['Logiciel']['id'],'id'=>'image_new_'.$logiciel['Logiciel']['id'])); ?>
                    </div>
<?php endforeach ?>
                    <div class="img-after-text">
                        <?php echo $this->Html->image('desing/img_i.png'); ?>
                    </div>
                    <div class="align-right-bot-txt">
                        Efficients<br />
                        Ergonomiques<br />
                        Indispensables
                    </div>
                    <div style="clear:both"></div>
                </div>
            </div>
            <div class="h-box-foot2"></div>
        </div>
        <div style="clear:both"></div>
</div>
<div class="text-h-container">
        <h2 id="bottom_title" data-pk='1'><?php echo $xml->getElementsByTagName('title_'.$lng)->item(0)->nodeValue ?></h2>
        
        <p data-pk='1'><?php echo $xml->getElementsByTagName('cnt_'.$lng)->item(0)->nodeValue ?></p>
</div>
<!-- ALL POPOVERS -->
<?php echo $this->element('acc_config'); ?>


<!-- ADMIN -->
<?php $url_bottomTitle = $this->Html->url(array('controller'=>'pages','action'=>'bottomTitle')) ?>
<?php $url_bottomCnt = $this->Html->url(array('controller'=>'pages','action'=>'bottomCnt')) ?>
<?php $posturl = $this->Html->url(array('controller'=>'pages','action'=>'acceuil')) ?>
<?php $d = $this->Html->url(array('controller'=>'logiciels','action'=>'prompted')); ?>
<?php $image_url =  $this->Html->url('/images/xsmall/') ?>

<!-- TEMPLATES -->
<div id="template" style="display: none;">
    <form class="form-inline editableform">
        <div class="control-group">
            <div>
                <div class="editable-input" style="text-align: right;">

                </div>
                <div class="editable-buttons"></div>
            </div>
            <div class="editable-error-block"></div>
        </div> 
    </form>
</div>
<div id='tpl' style="display: none;">
    <textarea style='width: 945px; height: 129px; margin-bottom: 5px;' name='cnt_fr' id='fr_cnt'></textarea>
    <textarea style='width: 945px; height: 129px; margin-bottom: 5px;' name='cnt_en' id='en_cnt'></textarea>
    <textarea style='width: 945px; height: 129px; margin-bottom: 5px;' name= 'cnt_es' id='es_cnt'></textarea>
</div>
<div id='tpl-1' style="display: none;">
    <input style='width: 945px; margin-bottom: 5px;' name='title_fr' type='text'></input>
    <input style='width: 945px; margin-bottom: 5px;' name='title_en' type='text'></input>
    <input style='width: 945px; margin-bottom: 5px;' name= 'title_es' type='text'></input>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var thm = $('#template').html();
        $.fn.editableform.template = thm;
///////////////////////////////////////////////////////////////////////////////////////////////
        $('.log').editable({
            ajaxOptions: {
                dataType: 'json',
                success: function(data, status,jsx){
                    $('#image_'+data.id).attr('src',"<?= $image_url ?>"+data.image);
                    $('#desc_'+data.id).text(data.description);
                    return status;
                }
            },
            name:'prompted',
            placement:'rigth',
            type:'select',
            title:'Logiciels',
            source:"<?= $d ?>",
            params: function(params){
                params.author = 'RATIANARIVO Mandrosohasina';
                params.dataOrg = $('.log').attr('data-org');
                return params;
            },
        });
///////////////////////////////////////////////////////////////////////////////////////////////
        $('.log_pros').editable({
            ajaxOptions: {
                dataType: 'json',
                success: function(data, status,jsx){
                    $('#image_'+data.id).attr('src',"<?= $image_url ?>"+data.image);
                    return status;
                }
            },
            name:'prompted',
            placement:'rigth',
            type:'select',
            title:'Logiciels',
            source:"<?= $d ?>",
            params: function(params){
                params.author = 'RATIANARIVO Mandrosohasina';
                params.dataOrg = $('.log_pros').attr('data-org');
                return params;
            },
        });
///////////////////////////////////////////////////////////////////////////////////////////////
        $('.log_news').editable({
            ajaxOptions: {
                dataType: 'json',
                success: function(data, status,jsx){
                    $('#image_new_'+data.id).attr('src',"<?= $image_url ?>"+data.image);
                    return status;
                }
            },
            name:'prompted',
            placement:'rigth',
            type:'select',
            title:'Logiciels',
            source:"<?= $d ?>",
            params: function(params){
                params.author = 'RATIANARIVO Mandrosohasina';
                params.dataOrg = $('.log_news').attr('data-org');
                return params;
            },
        });
///////////////////////////////////////////////////////////////////////////////////////////////
        $('.log').attr('href','#');
//////////////////////////////////////////////////////////////////////////////////////////////
        $('.text-h-container #bottom_title').editable({
            url: "<?= $url_bottomTitle ?>",
            mode: 'inline',
            type: 'text',
            tpl: $('#tpl-1').html(),
            showbuttons: 'bottom',
            params: function(params){
                params.title_fr = $('input[name=title_fr]').val();
                params.title_en = $('input[name=title_en]').val();
                params.title_es = $('input[name=title_es]').val();
                return params;
            }
        });
        $('.text-h-container #bottom_title').on('shown',function(){
            $('input[name=title_fr]').val("");
            $('input[name=title_en]').val("");
            $('input[name=title_es]').val("");
        });
//////////////////////////////////////////////////////////////////////////////////////////////
        $('.text-h-container p').editable({
            url: "<?= $url_bottomCnt ?>",
            mode: 'inline',
            type: 'textarea',
            tpl: $('#tpl').html(),
            showbuttons: 'bottom',
            params: function(params){
                params.cnt_fr = $('textarea[name=cnt_fr]').val();
                params.cnt_en = $('textarea[name=cnt_en]').val();
                params.cnt_es = $('textarea[name=cnt_es]').val();
                return params;
            }
        });
        $('.text-h-container p').on('shown',function(){
            $('textarea[name=cnt_fr]').val("");
            $('textarea[name=cnt_en]').val("");
            $('textarea[name=cnt_es]').val("");
        });
//////////////////////////////////////////////////////////////////////////////////////////////
    });
</script>
