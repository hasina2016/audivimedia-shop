            <nav class="navbar-aside navbar-static-side" role="navigation">
                <div class="sidebar-collapse nano">
                    <div class="nano-content">
                        <ul class="nav metismenu" id="side-menu">
                            
                            
                            <li class="dark-blue">
                                <a href="#"><span class="nav-label">Pages </span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="dropdown1"><a class="drop1" href="#">PAGES PRINCIPALES</a></li>
                                    <li class="dropdown1"><?php echo $this->Js->link('Acceuil', array('controller' => 'pages', 'action' => 'acceuil'), array('update'=>'#cnt')); ?></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Trouble Audition/Language</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Qui sommes-nous ?</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Léquipe</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Contact</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Mentions légales</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Condition générales de vente</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">PAGES LOGICIELS</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Famille</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Professionnels</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Orthophonistes</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">ORL et phoniatres</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Audioprothésistes</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Onglet logiciel</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">PAGES UTILISATEURS</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Se connecter</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Mot de passe oublié</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">S'inscrire</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Mes logiciels</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Mes commandes</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Modifier mon compte</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">PAGES COMMANDES</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Séléction des logiciels</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Coordonnées</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Moyen de paiement</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Paiement échoué</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Paiement réussi</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">GABARITS</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Menu principal</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Menu utilisateur</a></li>
                                    <li class="dropdown1"><a class="drop1" href="#">Pied de page</a></li>
                                </ul>
                            </li>
                            
                            
                            <li class="green">
                                <a href="#"><span class="nav-label">Utilisateurs</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="dropdown2"><?php echo $this->Js->link('Ajouter', array('controller' => 'users', 'action' => 'add'), array('update'=>'#cnt')); ?></li>
                                </ul>
                            </li>

                            <li class="orange">
                                <a href="#"><span class="nav-label">Commandes</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="dropdown3"><a href="#">Ajouter</a></li>    
                                    <li class="dropdown3"><?php echo $this->Js->link('Liste', array('controller' => 'orders', 'action' => 'index'), array('update'=>'#cnt')); ?></li>    
                                </ul>
                            </li>
                            
                            <li class="red">
                                <a href="#"><span class="nav-label">Logiciels</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="dropdown4"><?php echo $this->Js->link('AJOUTER', array('controller' => 'logiciels', 'action' => 'add'), array('update'=>'#cnt')); ?></li>
                                <?php foreach($logiciels as $logiciel): ?>
                                    <li class="dropdown4">
                                        <?php echo $this->Js->link($logiciel['Logiciel']['name_fr'], array('controller' => 'logiciels', 'action' => 'view',$logiciel['Logiciel']['id']), array('update'=>'#cnt')); ?>
                                    </li>
                                <?php endforeach; ?>
                                </ul>
                            </li>
                            
                            <li class="blue">
                                <a href="#"><span class="nav-label">Produits</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="dropdown4"><?php echo $this->Js->link('AJOUTER', array('controller' => 'products', 'action' => 'add'), array('update'=>'#cnt')); ?></li>
                                <?php foreach($products as $product): ?>
                                    <li class="dropdown4">
                                        <?php echo $this->Js->link($product['Product']['name_fr'], array('controller' => 'products', 'action' => 'view',$product['Product']['id']), array('update'=>'#cnt')); ?>
                                    </li>
                                <?php endforeach; ?>
                                </ul>
                            </li>
                            
                            <li class="purple">
                                <a href="#"><span class="nav-label">Paramètres</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <li class="dropdown6"><?php echo $this->Js->link('Images', array('controller'=>'media','action'=>'upload'), array('update'=>'#cnt')); ?></li>
                                    <li class="dropdown6"><?php echo $this->Js->link('Sogenactif', array('controller'=>'sogenactif','action'=>'index'), array('update'=>'#cnt')); ?></li>
                                </ul>
                            </li>

                                                                
                        </ul>
                    </div>
                </div>
            </nav>