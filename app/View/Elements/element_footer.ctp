<div class="footer">
        <div class="foot-separator-cont">
            <div class="foot-sep-color1"></div>
            <div class="foot-sep-color2"></div>
            <div class="foot-sep-color3"></div>
            <div class="foot-sep-color4"></div>
            <div class="foot-sep-color5"></div>
            <div class="foot-sep-color6"></div>
            <div class="foot-sep-color7"></div>
            <div style="clear:both"></div>
        </div>
        <div class="ft-content">
            <div class="ft-name">Sylvain Beaujouan Webdesigner</div>
            <div class="menu-foot">
                <div class="no-left-sep"><?php echo __('Centre auditif Audivi') ?></div>
                <div class="left-sep"><?php echo $this->Html->link(__('Mentions legales'), array('controller'=>'pages','action'=>'mentions'),array('class'=>'foot-url')) ?></div>
                <div class="left-sep">
                    <?php echo $this->Html->link(__('Conditions générales de vente'), array('controller'=>'pages','action'=>'cgv'),array('class'=>'foot-url')) ?>
                </div>
                <div class="left-sep"><?php echo $this->Html->link(__('Contacte'), array('controller'=>'pages','action'=>'contacte'),array('class'=>'foot-url')) ?></div>
                <div style="clear:both"></div>
            </div>
            <div class="copyright">&copy Audivimedia 2015</div>
        </div>
</div>