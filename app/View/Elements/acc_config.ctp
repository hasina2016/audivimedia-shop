<style type="text/css">
    textarea{
        color: black;
        columns: auto;
        width: 439px;
    }
    label{
        color: black;
        font-weight: bold;
    }
    #popover-content{
        max-width: 500px;
    }
    .popover-title{
        color: black;
        font-weight: bolder;
        background-color: #6D1F80;
        border-radius: 0px;
        color:white;
        text-align: center;
    }
    .popover{
        width: 471px;
        max-width: 471px;
        border-radius: 0px;
        background-color: 
    }
</style>

<!-- THIS IS THE TEMPLATE-->
<div style="display: none" id='thm'>
    <div class="popover" role="tooltip">
        <div class="arrow"></div>
        <h3 class="popover-title"></h3>
        <div class="popover-content"></div>
    </div>
</div>

<!-- --------------------------POPOVER LEFT--------------------------------- -->
<div id="popover-content_left" style="display: none;" >
<?php $url_setings= $this->Html->url(); ?>
    <div class="" style="padding-top: 10px;">
        <?php echo $this->Form->create('Config') ?>
        <div style="text-align: right;"><a href="#" id='save_left' class="btn btn-primary btn-xs save" style="margin-left: 15px">Sauver</a></div>
            <?php echo $this->Form->input('Français',array('type'=>'textarea','name'=>'fr','id'=>'fr','class'=>'form-controll','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('left_more_fr')->item(0)->nodeValue)) ?>
            <?php echo $this->Form->input('English',array('type'=>'textarea','name'=>'en','id'=>'en','class'=>'form-controll','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('left_more_en')->item(0)->nodeValue)) ?>
            <?php echo $this->Form->input('Español',array('type'=>'textarea','name'=>'es','id'=>'es','class'=>'form-controll','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('left_more_es')->item(0)->nodeValue)) ?>
        <?php echo $this->Form->end() ?>
        </div>
        <?php $url_config = $this->Html->url(array('controller'=>'pages','action'=>'config')) ?>
 <script type="text/javascript">
    $(function() {
        var tmplt = $('#thm').html();
///////////////////////////////////////////////////
        $('#leftdesc').popover({
            html: 'true',
            template: tmplt,
            placement: 'bottom',
            content : function() {
                return $('#popover-content_left').html();
            }
        });        
//////////////////////////////////////////////////
        $('#save_left').click(function(){
            $.ajax({
                type: 'Post',
                url: "<?= $url_config?>",
                dataType: 'json',
                data: {
                    'fr':   $('#fr').val(),
                    'en':   $('#en').val(),
                    'es':   $('#es').val(),
                    'id':   'left'     
                },
                success: function(data, status, jsx){
                    if(!$('#fr').val() == '') {$('#leftdesc').text($('#fr').val());};
                    $('.popover-content').html('');
                    $('.popover-title').text('Contenue Sauvegardé');
                }
            });
        });
        $('#leftdesc').on('shown.bs.popover',function(){
            
        });
    });
 </script>
 </div>

<!-- --------------------------POPOVER LEFT-BOTTOM--------------------------------- -->
<div id="popover-content_left-bottom" style="display: none;" >
<?php $url_setings= $this->Html->url(); ?>
    <div class="" style="padding-top: 10px;">
        <?php echo $this->Form->create('Config') ?>
        <div style="text-align: right;"><a href="#" id = 'save_purple' class="btn btn-primary btn-xs save" style="margin-left: 15px">Sauver</a></div>
            <?php echo $this->Form->input('Français',array('type'=>'textarea','name'=>'fr','id'=>'fr','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('left_more_fr')->item(0)->nodeValue)) ?>
            <?php echo $this->Form->input('English',array('type'=>'textarea','name'=>'en','id'=>'en','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('left_more_en')->item(0)->nodeValue)) ?>
            <?php echo $this->Form->input('Español',array('type'=>'textarea','name'=>'es','id'=>'es','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('left_more_es')->item(0)->nodeValue)) ?>
        <?php echo $this->Form->end() ?>
        </div>
        <?php $url_config = $this->Html->url(array('controller'=>'pages','action'=>'config')) ?>
 <script type="text/javascript">
    $(function() {
        var tmplt = $('#thm').html();
///////////////////////////////////////////////////
        $('#purpledesc').popover({
            html: 'true',
            template: tmplt,
            placement: 'bottom',
            content : function() {
                return $('#popover-content_left-bottom').html();
            }
        });        

//////////////////////////////////////////////////
        $('#save_purple').click(function(){
            $.ajax({
                type: 'Post',
                url: "<?= $url_config?>",
                dataType: 'json',
                data: {
                    'fr':   $('#fr').val(),
                    'en':   $('#en').val(),
                    'es':   $('#es').val(),
                    'id':   'left_more'     
                },
                success: function(data, status, jsx){
                    if (!$('#fr').val() == '') {$('#purpledesc').text($('#fr').val());};
                    $('.popover-content').html('');
                    $('.popover-title').text('Contenue Sauvegardé');
                }
            });
            

        });
    });
 </script>
 </div>

<!-- --------------------------POPOVER RIGTH--------------------------------- -->
<div id="popover-content_rigth" style="display: none;" >
<?php $url_setings= $this->Html->url(); ?>
    <div class="" style="padding-top: 10px;">
        <?php echo $this->Form->create('Config') ?>
        <div style="text-align: right;"><a href="#" id = 'save_rigth' class="btn btn-primary btn-xs save" style="margin-left: 15px">Sauver</a></div>
            <?php echo $this->Form->input('Français',array('type'=>'textarea','name'=>'fr','id'=>'fr','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('rigth_fr')->item(0)->nodeValue)) ?>
            <?php echo $this->Form->input('English',array('type'=>'textarea','name'=>'en','id'=>'en','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('rigth_en')->item(0)->nodeValue)) ?>
            <?php echo $this->Form->input('Español',array('type'=>'textarea','name'=>'es','id'=>'es','class'=>'form-controll','value'=>''.$xml->getElementsByTagName('rigth_es')->item(0)->nodeValue)) ?>
        <?php echo $this->Form->end() ?>
        </div>
        <?php $url_config = $this->Html->url(array('controller'=>'pages','action'=>'config')) ?>
 <script type="text/javascript">
    $(function() {
        var tmplt = $('#thm').html();
///////////////////////////////////////////////////
        $('#rigthdesc').popover({
            html: 'true',
            template: tmplt,
            placement: 'bottom',
            content : function() {
                return $('#popover-content_rigth').html();
            }
        });        

//////////////////////////////////////////////////
        $('#save_rigth').click(function(){
            $.ajax({
                type: 'Post',
                url: "<?= $url_config?>",
                dataType: 'json',
                data: {
                    'fr':   $('#fr').val(),
                    'en':   $('#en').val(),
                    'es':   $('#es').val(),
                    'id':   'rigth'     
                },
                success: function(data, status, jsx){
                    if (!$('#fr').val() == '') {$('#rigthdesc').text($('#fr').val());};
                    $('.popover-content').html('');
                    $('.popover-title').text('Contenue Sauvegardé');
                }
            });
            

        });
    });
 </script>
 </div>