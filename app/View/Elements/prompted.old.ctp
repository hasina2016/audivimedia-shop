<?php $lng = substr(Configure::read('Config.language'), 0,2); ?>
<div class="home-box-container">   
        <div class="home-box1">
            <div class="home-box-head1">
                <div class="box-h-big-title"><?php echo __('Espace Familles') ?></a></div>
                <div class="box-h-title"><span class='chng' id='leftdesc' ><?php echo $xml->getElementsByTagName('left_'.$lng)->item(0)->nodeValue ?></span></div>
            </div>
            <div class="h-box-content1"> 
                <div class="box-left-content">
                    <!-- prompted logiciels -->
                    <div>
                        <?php //echo $this->Html->image('desing/img_8.png'); ?> <!-- miniature -->
                        <?php echo $this->Html->image('/images/small/' . $logiciels[0]['Logiciel']['image']); ?>
                        <div class="red-title">
<?php echo $this->Html->link($logiciels[0]['Logiciel']['name_'.$lng], array('controller' => 'logiciels', 'action' => 'view',$logiciels[0]['Logiciel']['id']),array('update'=>'#cnt','class'=>'popup-button','title'=>'popup')); ?>
                        </div> <!-- nom du produits -->
                        <div class="blue-description"><?php echo $logiciels[0]['Logiciel']['description_'.$lng]; ?></div> <!-- description -->
                    </div>
                    <div>
                        <?php //echo $this->Html->image('desing/img_7.png'); ?>
                        <?php echo $this->Html->image('/images/small/' . $logiciels[1]['Logiciel']['image']); ?>
                        <div class="red-title">
<?php echo $this->Html->link($logiciels[1]['Logiciel']['name_'.$lng], array('controller' => 'logiciels', 'action' => 'view',$logiciels[1]['Logiciel']['id']),array('update'=>'#cnt','class'=>'popup-button','title'=>'popup')); ?>
                        </div>
                        <div class="blue-description"><?php echo $logiciels[0]['Logiciel']['description_'.$lng]; ?></div>
                    </div>
                    <div>
                        <?php //echo $this->Html->image('desing/img_6.png'); ?>
                        <?php echo $this->Html->image('/images/small/' . $logiciels[2]['Logiciel']['image']); ?>
                        <div class="red-title">
<?php echo $this->Html->link($logiciels[2]['Logiciel']['name_'.$lng], array('controller' => 'logiciels', 'action' => 'view',$logiciels[2]['Logiciel']['id']),array('update'=>'#cnt','class'=>'popup-button','title'=>'popup')); ?>
                        </div>
                        <div class="blue-description"><?php echo $logiciels[2]['Logiciel']['description_'.$lng]; ?></div>
                    </div>
                </div>
                <div class="box-right-content">
                    <div class="vert-purple-bar"></div>
                    <div class="purple-text"><span class="chng"><?php echo $xml->getElementsByTagName('left_more_'.$lng)->item(0)->nodeValue ?></span></div>
                    <div style="clear:both"></div>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="h-box-foot1"></div>
        </div>
        <div class="home-box2">
            <div class="home-box-head2">
                <div class="box-h-big-title"><?php echo __('Espace Professionnels') ?></div>
                <div class="box-h-title"><span class='chng'><?php echo $xml->getElementsByTagName('rigth_'.$lng)->item(0)->nodeValue ?></span></div>
            </div>
            
            <div class="h-box-content2">
                <div class="content-box2-left">
                    <?php echo $this->Html->image('desing/puce_vert.png'); ?> ORL & phoniatres<br />
                    <?php echo $this->Html->image('desing/puce_bleu.png'); ?> Audioprothésistes<br />
                    <?php echo $this->Html->image('desing/puce_bleu.png'); ?> Orthophonistes
                </div>
                
                <div class="content-box2-right">
                    <?php echo $this->Html->image('desing/casque.png'); ?>
                </div>
                
                <div style="clear:both"></div>
                
                <div class="img-list-content">
                    <div class="img-list">
                        <?php echo $this->Html->image('desing/img_1.png'); ?>
                    </div>
                    <div class="img-list">
                        <?php echo $this->Html->image('desing/img_2.png'); ?>
                    </div>
                    <div class="img-list">
                        <?php echo $this->Html->image('desing/img_3.png'); ?>
                    </div>
                    <div class="img-list">
                        <?php echo $this->Html->image('desing/img_4.png'); ?>
                    </div>
                    <div class="img-list">
                        <?php echo $this->Html->image('desing/img_5.png'); ?>
                    </div>
                    <div style="clear:both"></div>
                </div>
                
                <div class="content-box2-bottom-txt">
                    <?php echo $this->Html->image('desing/puce_vert.png'); ?> <?php echo __('Vos nouveaux outils !') ?>
                </div>
                
                <div class="img-list-content">
                    <div class="img-list">
                        <?php echo $this->Html->image('desing/img_6.png'); ?>
                    </div>
                    <div class="img-list">
                        <?php echo $this->Html->image('desing/img_7.png'); ?>
                    </div>
                    <div class="img-list">
                        <?php echo $this->Html->image('desing/img_8.png'); ?>
                    </div>
                    <div class="img-after-text">
                        <?php echo $this->Html->image('desing/img_i.png'); ?>
                    </div>
                    <div class="align-right-bot-txt">
                        Efficients<br />
                        Ergonomiques<br />
                        Indispensables
                    </div>
                    <div style="clear:both"></div>
                </div>
            </div>
            <div class="h-box-foot2"></div>
        </div>
        <div style="clear:both"></div>
</div>
<div class="text-h-container">
        <h2>Audivimédia logiciels & Audition</h2>
        
        <p>Plus de 15 années d'expérience !<br />
        Des logiciels utilisés et expérimentés dans de nombreux centres auditifs, cabinets, mais aussi par des familles.</p>
        
        <p>Créés par Alain Vinet, audioprothésiste membre du Collège National d'Audioprothèse et spécialisé dans l'appareillage de l'enfant, ils sont le fruit d'une passion pour l'excellence au service de la correction auditive.</p>
</div>
<?php //echo $this->Js->writeBuffer() ?>



<!-- ADMIN -->
<?php $posturl = $this->Html->url(array('controller'=>'pages','action'=>'acceuil')) ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#leftdesc').editable({
            type: 'textarea',
            url: '<?= $posturl?>',
            title: 'Description',
            placement: 'rigth'
        }
        );
    });
</script>
<?php //echo $left ?>
<?php //echo var_dump($xml) ?>
