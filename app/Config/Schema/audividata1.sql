-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 03 Mars 2016 à 10:04
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `audividata1`
--

-- --------------------------------------------------------

--
-- Structure de la table `autres`
--

CREATE TABLE IF NOT EXISTS `autres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detail_id` int(11) NOT NULL,
  `image` varchar(22) NOT NULL,
  `prix` int(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='produits informatique tel que cd, casques,...' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=7 ;

--
-- Contenu de la table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `active`, `created`, `modified`) VALUES
(1, 'Burton', 'burton', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(2, 'Celtek', 'celtek', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(3, 'Dakine', 'dakine', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(4, 'DC', 'dc', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(5, 'Electric', 'electric', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00'),
(6, 'Forum', 'forum', 1, '2012-12-06 00:00:00', '2012-12-06 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` decimal(6,2) DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `weight_total` decimal(6,2) DEFAULT NULL,
  `subtotal` decimal(6,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Contenu de la table `carts`
--

INSERT INTO `carts` (`id`, `sessionid`, `product_id`, `name`, `weight`, `price`, `quantity`, `weight_total`, `subtotal`, `created`, `modified`) VALUES
(1, 'fbbin9uj7debvp19nhpbfcaj74', 18, 'Le monde sonore d''Otto', NULL, '30.00', 2, NULL, '60.00', '2016-03-02 11:10:18', '2016-03-02 11:58:58'),
(2, 'fbbin9uj7debvp19nhpbfcaj74', 20, 'Cles en main ', NULL, '30.00', 1, NULL, '30.00', '2016-03-02 11:24:44', '2016-03-02 11:25:28'),
(3, 'efhpcekf19ui17mpo8vg36n7v7', 18, 'Le monde sonore d''Otto', NULL, '30.00', 25, NULL, '750.00', '2016-03-02 11:48:58', '2016-03-02 11:50:00'),
(4, 'efhpcekf19ui17mpo8vg36n7v7', 19, 'La vache et le chevalier', NULL, '302.00', 10, NULL, '3020.00', '2016-03-02 11:49:33', '2016-03-02 11:50:00'),
(9, 'jjofmjfbu5gt1h8rf2l8s8iav3', 18, 'Le monde sonore d''Otto', NULL, '30.00', 10, NULL, '300.00', '2016-03-02 12:37:52', '2016-03-02 12:37:52'),
(10, 'jjofmjfbu5gt1h8rf2l8s8iav3', 19, 'La vache et le chevalier', NULL, '302.00', 5, NULL, '1510.00', '2016-03-02 12:37:57', '2016-03-02 12:40:30'),
(11, 'jjofmjfbu5gt1h8rf2l8s8iav3', 20, 'Cles en main ', NULL, '30.00', 3, NULL, '90.00', '2016-03-02 12:38:02', '2016-03-02 12:38:02'),
(12, 'jjofmjfbu5gt1h8rf2l8s8iav3', 22, 'VideoShow', NULL, '375.00', 8, NULL, '3000.00', '2016-03-02 12:38:08', '2016-03-02 12:38:08'),
(14, 'tjes8r8dmo37nltb8rk8odfoc0', 18, 'Le monde sonore d''Otto', NULL, '30.00', 1, NULL, '30.00', '2016-03-02 12:56:48', '2016-03-02 12:56:48'),
(15, 'tjes8r8dmo37nltb8rk8odfoc0', 19, 'La vache et le chevalier', NULL, '302.00', 3, NULL, '906.00', '2016-03-02 12:56:52', '2016-03-02 12:56:52'),
(16, 'tjes8r8dmo37nltb8rk8odfoc0', 20, 'Cles en main ', NULL, '30.00', 5, NULL, '150.00', '2016-03-02 12:56:57', '2016-03-02 12:56:57'),
(17, 'tjes8r8dmo37nltb8rk8odfoc0', 22, 'VideoShow', NULL, '375.00', 8, NULL, '3000.00', '2016-03-02 12:57:01', '2016-03-02 12:57:01'),
(18, 'tjes8r8dmo37nltb8rk8odfoc0', 24, 'La souris bleue', NULL, '375.00', 1, NULL, '375.00', '2016-03-02 12:57:04', '2016-03-02 12:57:04'),
(27, 'efhpcekf19ui17mpo8vg36n7v7', 22, 'VideoShow', NULL, '375.00', 12, NULL, '4500.00', '2016-03-02 14:24:28', '2016-03-02 14:24:28'),
(28, 'efhpcekf19ui17mpo8vg36n7v7', 24, 'La souris bleue', NULL, '375.00', 8, NULL, '3000.00', '2016-03-02 14:24:35', '2016-03-02 14:24:35'),
(30, 'tu9nt63khgj18ttv9vlv3k6fu2', 18, 'Le monde sonore d''Otto', NULL, '30.00', 51, NULL, '1530.00', '2016-03-02 15:50:00', '2016-03-02 15:57:27'),
(31, 'q8bk1smi0c67mosbjddt7brmp5', 18, 'Le monde sonore d''Otto', NULL, '30.00', 7, NULL, '210.00', '2016-03-03 06:54:39', '2016-03-03 06:55:44'),
(32, 'q8bk1smi0c67mosbjddt7brmp5', 19, 'La vache et le chevalier', NULL, '302.00', 10, NULL, '3020.00', '2016-03-03 06:54:47', '2016-03-03 06:55:44'),
(40, '7mg88rqbfhv1bs04tqsbu2rhr2', 18, 'Le monde sonore d''Otto', NULL, '30.00', 7, NULL, '210.00', '2016-03-03 07:37:31', '2016-03-03 07:39:55'),
(41, '7mg88rqbfhv1bs04tqsbu2rhr2', 19, 'La vache et le chevalier', NULL, '302.00', 2, NULL, '604.00', '2016-03-03 07:38:33', '2016-03-03 07:39:36'),
(42, '7mg88rqbfhv1bs04tqsbu2rhr2', 21, 'Digivox', NULL, '375.00', 45, NULL, '9999.99', '2016-03-03 07:44:54', '2016-03-03 07:44:54'),
(43, '7mg88rqbfhv1bs04tqsbu2rhr2', 20, 'Cles en main ', NULL, '30.00', 10, NULL, '300.00', '2016-03-03 07:46:06', '2016-03-03 07:46:14'),
(44, 'lbvrgms2eefkas12u35mppahm5', 19, 'La vache et le chevalier', NULL, '302.00', 12, NULL, '3624.00', '2016-03-03 08:54:49', '2016-03-03 08:54:49'),
(45, 'lbvrgms2eefkas12u35mppahm5', 20, 'Cles en main ', NULL, '30.00', 45, NULL, '1350.00', '2016-03-03 08:54:59', '2016-03-03 08:54:59');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `lft` int(10) unsigned DEFAULT NULL,
  `rght` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_fr` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_fr` text COLLATE utf8_unicode_ci,
  `description_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description_es` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `lft`, `rght`, `name`, `name_fr`, `name_en`, `name_es`, `slug`, `description_fr`, `description_en`, `description_es`, `created`, `modified`) VALUES
(1, NULL, 1, 18, 'Pricipal', '', '', '', 'main', 'main', '', '', '2016-02-05 11:47:38', '2016-02-05 11:47:38'),
(2, 1, 2, 3, 'Famille', 'Famille', '', '', 'famille', 'Aidez votre enfant dÃ©ficient auditif\ndans son apprentissage de la langue', '', '', '2016-02-05 11:48:57', '2016-02-05 11:48:57'),
(3, 1, 4, 17, 'Professionnels', 'Professionnels', '', '', 'professionnels', 'Augmentez votre efficacitÃ©,\r\ndÃ©couvrez vos nouveaux outils', '', '', '2016-02-05 11:49:46', '2016-02-05 11:49:46'),
(4, 3, 11, 12, 'ORL et Phoniatres', 'Phoniatres', '', '', 'phoniatres', 'Des outils simples et ergonomiques Ã  avoir sous la main pour vos bilans audiomÃ©triques chez l''adulte et l''enfant ! ', '', '', '2016-02-09 12:52:13', '2016-02-09 12:52:13'),
(5, 3, 13, 14, 'Orthophonistes', 'Orthophoniastes', '', '', 'orthophonistes', 'Parce que la rÃ©Ã©ducation auditive passe aussi par le jeu ! BibliothÃ¨ques sonores, jeux, apprentissages ... ', '', '', '2016-02-09 12:55:47', '2016-02-09 12:55:47'),
(6, 3, 15, 16, 'AudioprothÃ©sistes', 'Audioprothesistes', '', '', 'audioprothesistes', 'Ayez sous la main les outils informatiques nÃ©cessaires Ã  parfaire vos bilans prÃ© et post-appareillage ! ', '', '', '2016-02-09 12:56:16', '2016-02-09 12:56:16');

-- --------------------------------------------------------

--
-- Structure de la table `details`
--

CREATE TABLE IF NOT EXISTS `details` (
  `category_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logiciel_id` int(10) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `details`
--

INSERT INTO `details` (`category_id`, `product_id`, `id`, `logiciel_id`, `page_id`) VALUES
(2, 18, 1, 18, 0),
(3, 18, 2, 18, 0),
(2, 20, 3, 20, 0),
(2, 19, 6, 19, 0),
(6, 21, 7, 21, 0),
(6, 22, 8, 22, 0),
(6, 23, 9, 23, 0),
(6, 24, 10, 24, 0),
(4, 21, 12, 21, 0),
(4, 22, 13, 22, 0);

-- --------------------------------------------------------

--
-- Structure de la table `dtmedia`
--

CREATE TABLE IF NOT EXISTS `dtmedia` (
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `logiciels`
--

CREATE TABLE IF NOT EXISTS `logiciels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_fr` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'short description',
  `about_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `about_es` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_es` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name_fr`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=25 ;

--
-- Contenu de la table `logiciels`
--

INSERT INTO `logiciels` (`id`, `category_id`, `brand_id`, `name_fr`, `name_en`, `name_es`, `slug`, `about_fr`, `about_en`, `about_es`, `description_fr`, `description_en`, `description_es`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'Otto''s world ', 'El mundo sonoror de Otto', 'dc-rogan-snowboard-boots-black-rasta', 'Jeu de d&eacute;couverte\nsonore et d&apos;&eacute;ducation\nuditive.', '', '', '', '', '', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 1, '2012-12-06 00:00:00', '2016-02-29 14:05:34', 1, 1, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', 'The cow and the knight', 'La vaca y el caballo', 'forum-aura-snowboard-boots-chocolate', 'Aide &acute; l&apos;acquisition de\nla langue fran&ccedil;aise. ', '', '', '', '', '', 'img_7.png', 'img_2.2.png', '302', 0, 10, NULL, 1199, 1, '2012-12-06 00:00:00', '2016-02-29 14:05:40', 1, 1, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', 'Keys in hand', '\nLlavero', 'dc-lear-mittens-blue-radiance-black', 'Familiarisation au code\nLPC.', '', '', 'Familiarise efficacement les adultes débutants et les enfants au code LPC sous la forme d''un jeu de cartes. ', '', '', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1138, 1, '2012-12-06 00:00:00', '2016-02-29 14:05:20', 1, 0, 1, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', 'Digivox', 'Digivox', 'Digivox', 'L''audiométrie vocale en toute fluidité !', '', '', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', '', '', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 5, 1, NULL, '2016-02-26 10:00:56', 1, 0, 0, 1, 0, 0, 0),
(22, 3, NULL, 'VideoShow', 'VideoShow', 'VideoShow', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 0, 1, NULL, '2016-02-26 10:02:57', 1, 0, 1, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', '\nThe terrace', '\nLa terraza', 'La Terrasse', 'Tests auditifs supraliminaires.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, '2016-02-26 09:52:35', 1, 0, 0, 1, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', '\nBlue mouse', 'ratón azul', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', '', '', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', '', '', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, '2016-02-26 09:51:25', 1, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `logiciel_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` decimal(8,2) unsigned DEFAULT '0.00',
  `order_item_count` int(11) DEFAULT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `tax` decimal(8,2) DEFAULT NULL,
  `shipping` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) unsigned DEFAULT NULL,
  `order_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorization` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `weight` decimal(8,2) unsigned DEFAULT '0.00',
  `price` decimal(8,2) unsigned DEFAULT NULL,
  `subtotal` decimal(8,2) unsigned DEFAULT NULL,
  `productmod_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `lft` int(10) NOT NULL,
  `rght` int(10) NOT NULL,
  `body_fr` text NOT NULL,
  `body_en` text NOT NULL,
  `body_es` text NOT NULL,
  `title` varchar(150) NOT NULL,
  `autor` varchar(150) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `logiciel_id` int(11) NOT NULL,
  `pos` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `lft`, `rght`, `body_fr`, `body_en`, `body_es`, `title`, `autor`, `active`, `product_id`, `logiciel_id`, `pos`) VALUES
(1, 0, 1, 42, 'Principale', 'Main', '?', 'Main', 'Admin', 1, 0, 0, 0),
(5, 1, 8, 9, '<p style="text-align:justify"><strong>Stimuler </strong>le d&eacute;veloppement sensoriel des enfants et am&eacute;liorer leurs facult&eacute;s auditives en leur faisant d&eacute;couvrir et identifier des sons. Un moment privil&eacute;gi&eacute; parents-enfant pour d&eacute;couvrir la richesse de l&#39;univers sonore. Test&eacute; et approuv&eacute; par des enfants d&eacute;ficients auditifs et leur famille !</p>\r\n', '<p style="text-align:justify">Stimulates the sensorial development of children and improve their hearing skills by making them discover and identify sounds. A special parent-child experience aimed at discovering the richness of the universe of sound. Tested and approved by hearing-&shy;impaired kids and their families!</p>\r\n', '<p style="text-align:justify">Estimular el desarrollo sensorial de los ni&ntilde;os y mejorar sus facultades auditivas haci&eacute;ndoles descubrir e identificar sonidos. Un momento privilegiado entre padres y ni&ntilde;os para descubrir el universo sonoro. &iexcl;Probado y aprobado por ni&ntilde;os con discapacidad auditiva y sus familias!</p>\r\n', 'But', '', 1, 0, 18, 2),
(6, 1, 10, 11, '<p>La vache et le chevalier a pour vocation d&rsquo;apporter une aide aux enfants sourds dans leur ma&icirc;trise de la langue fran&ccedil;aise.<img alt="" src="/audivimedia-shop/images/large/img_1.2.png" style="float:right; height:55px; width:49px" /></p>\r\n', '<p>timulates the sensorial development of children and improve their hearing skills by making them discover and identify sounds. A special parent-child experience aimed at discovering the richness of the universe of sound. Tested and approved by hearing-&shy;impaired kids and their families!</p>\r\n', '<p>Estimular el desarrollo sensorial de los ni&ntilde;os y mejorar sus facultades auditivas haci&eacute;ndoles descubrir e identificar sonidos. Un momento privilegiado entre padres y ni&ntilde;os para descubrir el universo sonoro. &iexcl;Probado y aprobado por ni&ntilde;os con discapacidad auditiva y sus familias!</p>\r\n', 'Objectif', '', 1, 0, 19, 1),
(7, 1, 12, 13, 'Stimule le dÃ©veloppement sensoriel des enfants et amÃ©liore leurs facultÃ©s auditives en leur faisant dÃ©couvrir et identifier des sons.\r\n\r\nOrganisÃ© sous forme de jeu, Le monde sonore d''Otto propose une grande variÃ©tÃ© d''environnements sonores de la vie quotidienne (la maison, la rue, le bord de mer, etc.).\r\nChaque environnement prÃ©sente une sÃ©rie de sons Ã  dÃ©couvrir, puis Ã  mÃ©moriser et Ã  reconnaÃ®tre Ã  travers plusieurs jeux. Les sons sont reprÃ©sentÃ©s Ã  l''Ã©cran par des images accompagnÃ©es du vocabulaire Ã©crit et prononcÃ©.', 'This educational game stimulates the sensory development of children and improves their hearing skills by making them discover and identify sounds.\r\n\r\nOtto''s World Of Sound offers a great variety of daily life sound environments (the home, the street, the seaside,etc.). Each environment presents a series of sounds to discover, memorize and recognize throughout several games.\r\nThe sounds are represented on screen by pictures together with written word and pronunciation.', 'Estimula el desarrollo sensorial de los niÃ±os y mejora sus facultades auditivas haciÃ©ndoles descubrir e identificar sonidos.\r\n\r\nEl mundo sonoro de Otto ofrece una gran variedad de entornos sonoros de la vida cotidiana (la casa, la calle, la orilla del mar, etc.).\r\n\r\nCada entorno contiene una serie de sonidos a descubrir, recordar y reconocer a travÃ©s de varios juegos. Los sonidos son representados en la pantalla por imÃ¡genes acompaÃ±adas del vocabulario escrito y oral. ', 'Descriptif', '', 1, 0, 18, 2),
(9, 1, 16, 17, 'Le monde sonore d''Otto a Ã©tÃ© dÃ©veloppÃ© par AudivimÃ©dia avec l''aide de la firme danoise Oticon dans le cadre de son programme OtiKids.', 'Prerequisites : Children eager to discover sounds and/or in need of auditory re-education. ', '', 'Info Pratique', '', 1, 0, 18, 2),
(10, 1, 18, 19, 'DÃ©vÃ©dÃ©rom conÃ§u spÃ©cifiquement pour les enfants sourds, autour dâ€™une histoire transposÃ©e en trois niveaux de langue, qui leur permet une progression par paliers.\r\n\r\nRÃ©alisÃ© avec lâ€™aide du MinistÃ¨re de lâ€™Education nationale.\r\n\r\nLâ€™histoire de "La vache et le chevalier" et les parties annexes du dÃ©vÃ©dÃ©rom sont sous-titrÃ©es, codÃ©es en LPC, signÃ©es (pour le premier niveau de la langue uniquement) et illustrÃ©es de nombreux visuels aidant Ã  la comprÃ©hension.\r\n\r\nUne partie lexicale et syntaxique se rÃ©fÃ©rant Ã  lâ€™histoire ainsi quâ€™un thÃ¨me dÃ©veloppÃ© sur "lâ€™ombre et la lumiÃ¨re" sont proposÃ©s sur la partie interactive du dÃ©vÃ©dÃ©rom. ', 'This interactive DVD features a story transposed in three levels of language, which allows children to move progressively through the DVD.\r\n\r\nCreated with the support of the National Ministry of Education.\r\n\r\nThe story "La Vache et le Chevalier" and the additional parts of the DVD are subtitled, LPC coded, and illustrated with numerous images for a better understanding.\r\n\r\nThe interactive rubric of the DVD includes a lexical and syntactic story related part and a theme developed around "Light and Shadow". ', 'El DVD-ROM estÃ¡ diseÃ±ado en torno a una historia estructurada en tres niveles de lenguaje, que permite a los niÃ±os progresar por etapas.\r\n\r\nRealizado con la ayuda del Ministerio de EducaciÃ³n Nacional.\r\n\r\nLa historia de â€œLa vaca y el caballeroâ€ y las partes anexas del DVD-ROM estÃ¡n subtituladas, codificadas en LPC, suscritas (Ãºnicamente para el primer nivel de lengua) e ilustradas con numerosas imÃ¡genes que ayudan a la comprensiÃ³n.\r\n\r\nEn la parte interactiva del DVD-ROM se encuentran disponibles una parte lÃ©xica y sintÃ¡ctica en referencia a la historia, asÃ­ como un tema desarrollado sobre â€œlas luces y las sombrasâ€ ', 'Descriptif', '', 1, 0, 19, 2),
(11, 1, 20, 21, '<p>Les &ldquo;Livres compl&eacute;t&eacute;s&rdquo; constituent une aide pour les enseignants, en leur apportant un support adapt&eacute; &agrave; la surdit&eacute; de l&rsquo;enfant, afin de favoriser la ma&icirc;trise de la langue fran&ccedil;aise chez leurs &eacute;l&egrave;ves sourds profonds ou malentendants : les r&eacute;cits sont progressifs, pr&eacute;cis&eacute;ment illustr&eacute;s, les modes de communication sont sp&eacute;cifiques (code LPC*, LSF*, sous-titrage... ).</p>\r\n\r\n<p>Les &ldquo;Livres compl&eacute;t&eacute;s&rdquo; ont pour base une histoire propos&eacute;e en trois niveaux de langue, donnant la possibilit&eacute; d&rsquo;une progression par paliers.</p>\r\n\r\n<p>Dans les &ldquo;Livres compl&eacute;t&eacute;s&rdquo;, les illustrations suivent pr&eacute;cis&eacute;ment les textes pour faciliter l&rsquo;acc&egrave;s au sens. Le c&eacute;d&eacute;rom interactif de &ldquo;La vache et le chevalier&rdquo;, bien qu&rsquo;il soit con&ccedil;u sp&eacute;cifiquement pour les enfants sourds profonds et malentendants - donc enti&egrave;rement sous-titr&eacute; et compl&eacute;t&eacute; par le code LPC (Langue fran&ccedil;aise Parl&eacute;e Compl&eacute;t&eacute;e), est un support qui peut &ecirc;tre montr&eacute; &agrave; toute la classe.</p>\r\n\r\n<p>Dans les &ldquo;Livres compl&eacute;t&eacute;s&rdquo; , les adaptations au handicap de la surdit&eacute; ne sont pas syst&eacute;matiquement impos&eacute;es : elles constituent des options, permettant ainsi une souplesse dans les modalit&eacute;s propos&eacute;es par l&rsquo;enseignant selon le profil de l&rsquo;enfant et selon ses comp&eacute;tences en langue fran&ccedil;aise et en code LPC. Un compl&eacute;ment lexical et le d&eacute;veloppement d&rsquo;un th&egrave;me r&eacute;aliste viennent enrichir l&rsquo;histoire (ici, le th&egrave;me porte sur &ldquo;L&rsquo;ombre et la lumi&egrave;re&rdquo;).</p>\r\n\r\n<p style="text-align: justify;">A terme, Les &ldquo;Livres compl&eacute;t&eacute;s&rdquo; doivent constituer une collection destin&eacute;e &agrave; des &eacute;l&egrave;ves sourds profonds ou malentendants, de la maternelle au CM2. Infos pratiques Le d&eacute;v&eacute;d&eacute;rom contient &eacute;galement un guide p&eacute;dagogique et des activit&eacute;s autour du th&egrave;me de l&rsquo;ombre et de la lumi&egrave;re. ! Pr&eacute;requis : Jeunes enfants sourds profonds ou malentendants. 5/10 ans Selon le niveau en fran&ccedil;ais de l&rsquo;enfant. PC Configuration minimum : Pentium II 350 Mhz, 64Mo de RAM, Windows XP, Vista, seven et 8, carte vid&eacute;o 16 Mo, carte son, lecteur DVD-ROM, &eacute;cran 1024 x 768 Des logiciels adapt&eacute;s pour votre enfant sourd ! Le monde sonore d&#39;Otto Stimule le d&eacute;veloppement sensoriel et am&eacute;liore les facult&eacute;s auditives !en savoir plus Jeu de d&eacute;couverte sonore et d&rsquo;&eacute;ducation auditive. 2+ ans Le monde sonore d&#39;Otto La vache et le chevalier Aide les enfants sourds dans leur ma&icirc;trise de la langue fran&ccedil;aise. Histoire sous-titr&eacute;e et cod&eacute;e en LPC.en savoir plus Aide &agrave; l&#39;acquisition de la langue fran&ccedil;aise. A l&#39;attention des enseignants. 5/10 ans La vache et le chevalier Cl&eacute;s en main Familiarise efficacement les adultes d&eacute;butants et les enfants au code LPC sous la forme d&rsquo;un jeu de cartes.en savoir plus Familiarisation au code LPC. 5+ ans Cl&eacute;s en main</p>\r\n', '<p style="text-align: justify;">&quot;The &quot;&quot;completed books&quot;&quot; constitute an aid for the teachers, providing them with relevant support adapted to the deafness of the child, with the purpose of assisting the students with profound deafnessor hearing impairment in the learning of the French language: the stories are progressive, adequately illustrated, and the ways of communication are specific (LPC* code, LSF*, subtitles...) The &quot;&quot;Completed Books&quot;&quot; are based on a story proposed in three levels of language, offering the possibility of a progression by level. In the &quot;&quot;Completed Books&quot;&quot;, the illustrations follow the texts in a precise way to facilitate learning. The Cow And The Knight interactive DVD has been designed specifically for children with profound deafness or hearing impairment therefore, it is subtitled and complemented with LPC code, being a support that can be shown to the entire class. In the &quot;&quot;Completed Books&quot;&quot;, the adaptations for deafness are not systematically imposed: they are featured as options, allowing for flexibility in the modalities offered by the teacher, according to the child&#39;s profile and their skills with the French language and LPC code. A lexical part and the development of a realistic topic, help enhance the story (in this case, the realistic topic is that of &quot;&quot;The lights and the shadows&quot;&quot;). Finally, the &quot;&quot;Completed Books&quot;&quot; should constitute a collection for students with profound deafness or hearing impairment, from preâ€school to elementary. &quot;</p>\r\n', '<p style="text-align: justify;">Los &ldquo;libros completados&rdquo; constituyen una ayuda para los profesores, proporcionando les, un buen soporte, adaptado a las necesidades de los ni&ntilde;os con deficiencia auditiva, con el fin de favorecer el aprendizaje y dominio de la lengua espa&ntilde;ola. Les hist&oacute;rias, est&aacute;n estructuradas de manera progresiva, est&aacute;n ilustradas de una manera muy precisa e incluyen subt&iacute;tulos, c&oacute;digo LPC y LSF. El DVD-ROM interactivo de &ldquo;La vaca y el caballero&rdquo;, ha sido dise&ntilde;ado espec&iacute;ficamente para los ni&ntilde;os con sordera profunda o discapacidad auditiva, por tanto, est&aacute;n subtitulados y complementados con el c&oacute;digo LPC, es un apoyo que puede ser mostrado a toda la clase. En los &ldquo;Libros completados&rdquo; , las adaptaciones a la discapacidad de la sordera no est&aacute;n sistem&aacute;ticamente impuestas: hay diferentes opciones que permiten una flexibilidad en las modalidades ofrecidas por el docente, seg&uacute;n el perfil del ni&ntilde;o y seg&uacute;n sus capacidades en la lengua espa&ntilde;ola y en el c&oacute;digo LPC. Un complemento l&eacute;xico y el desarrollo de un tema realista enriquecen la historia (aqu&iacute;, el tema trata sobre &ldquo;Las luces y las sombras&rdquo;). Finalmente, Los &ldquo;Libros completados&rdquo; deben constituir una colecci&oacute;n destinada a los alumnos con sordera profunda o discapacidad, desde infantil hasta primaria.</p>\r\n', 'Detail', '', 1, 0, 19, 1),
(12, 1, 22, 23, 'ClÃ©s en main est un outil spÃ©cifique pour familiariser et entraÃ®ner les enfants sourds Ã  la pratique du Code LPC. ', 'Ready To Go is a specific tool used in familiarisation training for both beginner adults and deaf children in the practice of the LPC Code (this code helps the deaf learn the French language).', 'Listo para usar es una herramienta para familiarizar y entrenar a los niÃ±os sordos en la prÃ¡ctica del CÃ³digo LPC ', 'Objectif', '', 1, 0, 20, 0),
(13, 1, 24, 25, 'ClÃ©s en main se prÃ©sente sous la forme de jeux de cartes affichant les phonÃ¨mes. Ils sont Ã©crits ou bien codÃ©s par une tÃªte virtuelle.\r\nVous pourrez pratiquer le codage ou le dÃ©codage du code LPC, avec ou sans la vocalisation des phonÃ¨mes.\r\n\r\nClÃ©s en main stimule le dÃ©veloppement sensoriel de ces enfants et amÃ©liore leurs facultÃ©s auditives en leur faisant dÃ©couvrir et identifier des sons. ', 'Ready To Go is a card game depicting phonemes, which are written or modified by the program. This allows the user to practice coding or decoding the LPC code, with or without vocalizing the phonemes.\r\n\r\nReady To Go stimulates the sensory development of children and helps improve their hearing skills, making them discover and identify sounds. ', 'Listo para usar se presenta en forma de juegos de cartas que muestra fonemas. EstÃ¡n escritos o bien modificados por una cabeza virtual.\r\n\r\nUsted podrÃ¡ practicar la codificaciÃ³n o descodificaciÃ³n del cÃ³gido LPC, con o sin vocalizaciÃ³n des los fonemas.\r\n\r\nListos para usar estimula el desarrollo sensorial de estos niÃ±os y mejora sus facultades y mejora sus facultades auditivas haciÃ©ndole descubrir e identificar sonidos. ', 'Descriptif', '', 1, 0, 20, 2),
(14, 1, 26, 27, 'ClÃ©s en main sâ€™organise selon une progression simple qui commence avec les positions des voyelles sur le visage, pour arriver aux syllabes ("consonne LPC" + "voyelle LPC") et aux mots.\r\n\r\nPour des exercices progressifs ClÃ©s en main offre la possibilitÃ© de ne travailler quâ€™avec certaines voyelles ou certaines consonnes.\r\n\r\nVous pouvez composer des suites de voyelles ou de syllabes, choisir le nombre de cartes (donc le nombre de "clÃ©s LPC" pour les exercices), accÃ©der Ã  des tableaux rÃ©capitulatifs, choisir un mot dans une listeâ€¦\r\n\r\nVous avez aussi le choix entre le mode silencieux ou sonore (exceptÃ© pour les exercices sur les mots qui sont toujours silencieux).\r\n\r\nAvec ClÃ©s en main, vous avez un outil complet pour faire progresser rapidement votre enfant. ', '"Ready To Go is organized according to a simple progression that starts with the position of the vowels on a face, followed by the syllables (""LPC consonant"" and ""LPC vowel"") and finally the words.\r\n\r\nFor progressive exercises, Ready To Go offers the possibility to work only with a given set of vowels and consonants.\r\n\r\nReady To Go offers the possibility to form sequences of vowels and syllables, choose the number of cards (according to the number of ""LPC keys"" for the exercises), access summary tables, choose a word from a listâ€¦\r\n\r\nIt also allows the user to choose between the silent or sound mode (except for the exercises with words that are always silent).\r\n\r\nWith Ready To Go you will have a thorough tool to help your child progress rapidly."\r\n\r\n', ' Listo para usar se organiza segÃºn una progresiÃ³n simple que comienza con las posiciones de la vocales en el rostro, para llegar a las sÃ­labas ("consonante LPC" + "vocal LPC") y a las palabras.\r\n\r\nPara ejercicios progresivos, Listos para usar ofrece la posibilidad de trabajar solamente determinadas vocales o consonantes.\r\n\r\nPuede componer secuencias de vocales o sÃ­labas, elegir el nÃºmero de cartas (por tanto, el nÃºmero de "llaves LPC" para los ejercicios), acceder a tablas de resumen, elegir una palabra en la listaâ€¦\r\n\r\nTambiÃ©n tiene la posibilidad de elegir entre el modo silencioso o sonoro (a excepciÃ³n de los ejercicio con palabras que siempre son silenciosas).\r\n\r\nCon Listos para usar, dispone de una herramienta completa para hacer progresar rÃ¡pidamente a su hijo.\r\nInformaciones prÃ¡cticas\r\n\r\n!\r\n\r\nRequisitos previos : Para los adultos principiantes y niÃ±os sordos con sus padres\r\n\r\n5+\r\naÃ±os\r\n\r\nSegÃºn el nivel de francÃ©s del niÃ±o.\r\n\r\nPC\r\n\r\nConfiguraciÃ³n mÃ­nima : Pentium II 350 Mhz, 64Mo de RAM, Windows XP, Vista, siete y 8, tarjeta de vÃ­deo 16 Mo, tarjeta de sonido, lector DVD-ROM, pantalla 1024 x 768\r\nLa vache et le chevalier\r\nOfrece una ayuda a los niÃ±os sordos, en su conocimiento de la lengua espaÃ±ola.\r\nMeta\r\n\r\n"La Vache et le Chevalier" ofrece una ayuda a los niÃ±os sordos en su conocimiento de la lengua espaÃ±ola.\r\nÃ©cran 1 Ã©cran 2\r\n\r\n30 â‚¬\r\ninc.IVA\r\n\r\nÂ¡Compra!\r\n\r\nEl software estÃ¡ disponible unicamente en CD-ROM.\r\nDescripciÃ³n\r\n\r\nEl DVD-ROM estÃ¡ diseÃ±ado en torno a una historia estructurada en tres niveles de lenguaje, que permite a los niÃ±os progresar por etapas.\r\n\r\nRealizado con la ayuda del Ministerio de EducaciÃ³n Nacional.\r\n\r\nLa historia de â€œLa vaca y el caballeroâ€ y las partes anexas del DVD-ROM estÃ¡n subtituladas, codificadas en LPC, suscritas (Ãºnicamente para el primer nivel de lengua) e ilustradas con numerosas imÃ¡genes que ayudan a la comprensiÃ³n.\r\n\r\nEn la parte interactiva del DVD-ROM se encuentran disponibles una parte lÃ©xica y sintÃ¡ctica en referencia a la historia, asÃ­ como un tema desarrollado sobre â€œlas luces y las sombrasâ€\r\nDetalles\r\n\r\nLos â€œlibros completadosâ€ constituyen una ayuda para los profesores, proporcionando les, un buen soporte, adaptado a las necesidades de los niÃ±os con deficiencia auditiva, con el fin de favorecer el aprendizaje y dominio de la lengua espaÃ±ola. Les histÃ³rias, estÃ¡n estructuradas de manera progresiva, estÃ¡n ilustradas de una manera muy precisa e incluyen subtÃ­tulos, cÃ³digo LPC y LSF.\r\n\r\nEl DVD-ROM interactivo de â€œLa vaca y el caballeroâ€, ha sido diseÃ±ado especÃ­ficamente para los niÃ±os con sordera profunda o discapacidad auditiva, por tanto, estÃ¡n subtitulados y complementados con el cÃ³digo LPC, es un apoyo que puede ser mostrado a toda la clase.\r\n\r\nEn los â€œLibros completadosâ€ , las adaptaciones a la discapacidad de la sordera no estÃ¡n sistemÃ¡ticamente impuestas: hay diferentes opciones que permiten una flexibilidad en las modalidades ofrecidas por el docente, segÃºn el perfil del niÃ±o y segÃºn sus capacidades en la lengua espaÃ±ola y en el cÃ³digo LPC.\r\n\r\nUn complemento lÃ©xico y el desarrollo de un tema realista enriquecen la historia (aquÃ­, el tema trata sobre â€œLas luces y las sombrasâ€).\r\n\r\nFinalmente, Los â€œLibros completadosâ€ deben constituir una colecciÃ³n destinada a los alumnos con sordera profunda o discapacidad, desde infantil hasta primaria.\r\nInformaciones prÃ¡cticas\r\n\r\n!\r\n\r\nRequisitos previos : NiÃ±os pequeÃ±os con sordera profunda o discapacidad auditiva.\r\n\r\n5/10\r\naÃ±os\r\n\r\nSegÃºn el nivel de francÃ©s del niÃ±o.\r\n\r\nPC\r\n\r\nConfiguraciÃ³n mÃ­nima : Pentium II 350 Mhz, 64Mo de RAM, Windows XP, Vista, siete y 8, tarjeta de vÃ­deo 16 Mo, tarjeta de sonido, lector DVD-ROM, pantalla 1024 x 768\r\nDes logiciels adaptÃ©s pour votre enfant sourd !\r\nEl mundo sonoro de Otto\r\n\r\nEstimula el desarrollo sensorial de los niÃ±os y mejora sus facultades auditivas !saber mÃ¡s\r\n\r\nJuego de descubrimiento de los sonidos y educaciÃ³n auditiva.\r\n\r\n2+\r\naÃ±os\r\nEl mundo sonoro de Otto\r\nLa vache et le chevalier\r\n\r\nAyuda a los niÃ±os sordos en su conocimiento de la lengua francesa. Historia subtitulada y codificada en LPCsaber mÃ¡s\r\n\r\nOfrece una ayuda a los niÃ±os sordos, en su conocimiento de la lengua espaÃ±ola.\r\n\r\n5/10\r\naÃ±os\r\nLa vache et le chevalier\r\nClÃ©s en main\r\n\r\nFamiliariza eficazmente a los adultos principiantes y a los niÃ±os con el cÃ³digo LPC a travÃ©s de un juego de cartas saber mÃ¡s\r\n\r\nFamiliarizaciÃ³n con el cÃ³digo LPC\r\n\r\n5+\r\naÃ±os\r\nClÃ©s en main\r\n', 'Detail', '', 1, 0, 20, 1),
(21, 1, 40, 41, '<p style="text-align: justify;">UN JEU MOTIVANT...<br />\r\n<br />\r\nDes graphiques anim&eacute;s captivent l&#39;enfant ; sa curiosit&eacute; le conduit &agrave; relever chaque nouveau d&eacute;fi. Le logiciel enregistre sa progression et adapte la complexit&eacute; des activit&eacute;s &agrave; l&#39;exp&eacute;rience qu&#39;il a acquise.<br />\r\n&Agrave; l&#39;issue de chaque &eacute;tape, l&#39;enfant re&ccedil;oit une r&eacute;compense : une pi&egrave;ce &agrave; placer sur un puzzle. Quand le puzzle sera complet, il aura d&eacute;montr&eacute; sa capacit&eacute; &agrave; reconna&icirc;tre tous les sons du jeu.<br />\r\n<br />\r\n... ET EFFICACE !<br />\r\n<br />\r\nLes premi&egrave;res exp&eacute;rimentations du logiciel Le monde sonore d&#39;Otto, men&eacute;es par des orthophonistes avec des enfants de 3 &agrave; 12 ans, ont donn&eacute; des r&eacute;sultats tr&egrave;s positifs : au bout de deux mois, &agrave; raison de trois courtes s&eacute;ances hebdomadaires, certains enfants pr&eacute;sentant une d&eacute;ficience auditive s&eacute;v&egrave;re-profonde ont atteint une reconnaissance de 100 % des sons du jeu.</p>\r\n', '<p style="text-align: justify;">A MOTIVATING GAME&hellip;<br />\r\n<br />\r\n&quot;While animations draw children&#39;s attention, their curiosity leads them to achieve new challenges. The software keeps record of their progress and adapts the complexity of the activities to the experience the child has acquired.<br />\r\nAt the end of each step, the child receives a reward: a new puzzle piece to place in the puzzle. Once the puzzle is completed, the child will has demonstrated its capacity to recognize all the sounds of the game. &quot;<br />\r\n<br />\r\n...AND EFFICIENT&hellip;!<br />\r\n<br />\r\n<br />\r\n&quot;The first experiences of Otto&#39;s World Of Sound were performed by speech<br />\r\ntherapists with children ranging between 3 and 12 years of age and has shown very positive results. After two months of three short weekly sessions, some children with profound hearing loss have been able to recognize 100% of the sounds in the game. &quot;<br />\r\n&nbsp;</p>\r\n', '<p style="text-align: justify;">UN JUEGO ESTIMULANTE...<br />\r\n<br />\r\nLos gr&aacute;ficos animados atraen la atenci&oacute;n del ni&ntilde;o y su curiosidad le conducir&aacute; a lograr cada uno de los retos. El software registra su progreso y adapta la complejidad de las actividades a la experiencia que ha adquirido.<br />\r\nAl final de cada etapa, el ni&ntilde;o recibe una recompensa: una pieza para colocar sobre un puzzle. Cuando el puzzle estar&aacute; completo, habr&aacute; demostrado su capacidad para reconocer todos los sonidos del juego.<br />\r\n<br />\r\n... Y EFICAZ!<br />\r\n<br />\r\nLas pruebas del software El mundo sonoro de Otto, han sido realizadas por logopedas con ni&ntilde;os de 3 a 12 a&ntilde;os, los resultados han sido muy positivos: al cabo de dos meses, a raz&oacute;n de tres sesiones cortas semanales, algunos ni&ntilde;os con deficiencia auditiva severa han logrado reconocer el 100 % de los sonidos del juego.</p>\r\n', 'Detail', '', 1, 0, 18, 1);

-- --------------------------------------------------------

--
-- Structure de la table `paragraphes`
--

CREATE TABLE IF NOT EXISTS `paragraphes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) DEFAULT NULL,
  `body_fr` text,
  `body_en` text NOT NULL,
  `body_es` text NOT NULL,
  `image` text,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `productmods`
--

CREATE TABLE IF NOT EXISTS `productmods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `weight` decimal(8,2) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modified` (`modified`),
  KEY `category_id` (`product_id`),
  KEY `brand_id` (`value`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=2 ;

--
-- Contenu de la table `productmods`
--

INSERT INTO `productmods` (`id`, `product_id`, `sku`, `name`, `value`, `price`, `weight`, `active`, `views`, `created`, `modified`) VALUES
(1, 19, 'aura_boot_8', 'Size  8 US', 'Size  8 US', '68.95', '5.00', 1, 0, '2013-10-30 00:11:30', '2013-10-30 00:11:30');

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name_fr` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_fr` varchar(250) NOT NULL COMMENT 'short description',
  `about_en` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `about_es` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` text,
  `description_en` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description_es` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name_fr`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=25 ;

--
-- Contenu de la table `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `name_fr`, `name_en`, `name_es`, `slug`, `about_fr`, `about_en`, `about_es`, `description_fr`, `description_en`, `description_es`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'Otto''s world ', 'El mundo sonoror de Otto', 'dc-rogan-snowboard-boots-black-rasta', 'Stimule le développement sensoriel et améliore les facultés auditives !', '', '', 'Jeu de découverte sonore et d’éducation auditive.', '', '', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 0, '2012-12-06 00:00:00', '2016-02-10 07:34:32', 1, 0, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', '', '', 'forum-aura-snowboard-boots-chocolate', 'Aide les enfants sourds dans leur maitrise de la lange Française. Histoire sous-titrée et condé en LPC. ', '', '', 'Aide à l''acquisition de la langue française. A l''attention des enseignants.', '', '', 'img_7.png', 'img_2.2.png', '302', 0, 10, NULL, 1199, 0, '2012-12-06 00:00:00', '2016-02-18 13:36:27', 1, 0, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', '', '', 'dc-lear-mittens-blue-radiance-black', '\n	\nFamiliarise efficacement les adultes débutants et les enfants au code LPC sous la forme d''un jeu de cartes. ', '', '', 'Entrainement et familiarisation au code LCP', '', '', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1139, 1, '2012-12-06 00:00:00', '2016-02-10 07:33:56', 1, 0, 0, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', '', '', 'Digivox', 'L''audiométrie vocale en toute fluidité !', '', '', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', '', '', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 5, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(22, 3, NULL, 'VideoShow', '', '', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', '', '', 'La Terrasse', 'Tests auditifs supraliminaires.', '', '', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', '', '', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', '', '', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', '', '', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', '', '', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `products.old`
--

CREATE TABLE IF NOT EXISTS `products.old` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `brand_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'short description',
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_max` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `try` tinyint(1) NOT NULL DEFAULT '0',
  `trydur` int(2) NOT NULL DEFAULT '10',
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `prompted` tinyint(1) NOT NULL DEFAULT '0',
  `cdrom` tinyint(1) NOT NULL,
  `usb` tinyint(1) NOT NULL,
  `telechargement` tinyint(1) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `mac` tinyint(1) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `modified` (`modified`),
  KEY `name_slug` (`slug`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Logiciels informatique' AUTO_INCREMENT=25 ;

--
-- Contenu de la table `products.old`
--

INSERT INTO `products.old` (`id`, `category_id`, `brand_id`, `name`, `slug`, `about`, `description`, `image`, `image_max`, `price`, `try`, `trydur`, `tags`, `views`, `active`, `created`, `modified`, `prompted`, `cdrom`, `usb`, `telechargement`, `win`, `mac`, `age`) VALUES
(18, 4, 4, 'Le monde sonore d''Otto', 'dc-rogan-snowboard-boots-black-rasta', '', 'this is a description', 'img_8.png', 'img_1.2.png', '30', 0, 10, NULL, 1179, 1, '2012-12-06 00:00:00', '2016-02-10 07:34:32', 1, 0, 0, 0, 0, 0, 0),
(19, 4, 6, 'La vache et le chevalier', 'forum-aura-snowboard-boots-chocolate', '', 'this is a description', 'img_7.png', 'img_2.2.png', '30', 0, 10, NULL, 1199, 0, '2012-12-06 00:00:00', '2016-02-10 07:34:14', 1, 0, 0, 0, 0, 0, 0),
(20, 4, 4, 'Cles en main ', 'dc-lear-mittens-blue-radiance-black', '', 'Entrainement et familiarisation au code LCP', 'img_6.png', 'img_3.2.png', '30', 0, 10, NULL, 1139, 1, '2012-12-06 00:00:00', '2016-02-10 07:33:56', 1, 0, 0, 0, 0, 0, 0),
(21, 3, NULL, 'Digivox', 'Digivox', 'L''audiométrie vocale en toute fluidité !', 'Logiciel très complet et ultra ergonomique pour la passation d''audiométries vocales avec ou sans bruit de fond, de la stéréo au 7.1 Digivox contient nativement toutes les listes validées par le Collè National d''Audioprothèse et se veut être l''outil à avoir en permanence sous la main pour la réalisation d''audiométries vocales. Petit, simple et ultra complet !\r\n', NULL, 'img-aud_1.png', '375', 1, 10, NULL, 24, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(22, 3, NULL, 'VideoShow', 'VidéoShow', 'Support logiciel à l''audiométrie pédiatrique.', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', NULL, 'img-aud_2.png', '375', 0, 10, NULL, 3, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 3, NULL, 'La Terrasse', 'La Terrasse', 'Tests auditifs supraliminaires.', 'Optimisez les réglages dans la dynamique résiduelle de vos patients. Soyez pédagogique avec vos patients et leurs accompagnants. Evaluez la discrimination auditive des enfants sous forme ludique !', NULL, 'img-aud_4.png', '375', 0, 10, NULL, 1, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(24, 3, NULL, 'La souris bleue', 'La souris bleue', 'L''imagier sonore pour l''éveil auditif !', 'L''imagier sonore La Souris bleue est un logiciel d''éveil auditif et d''évaluation des capacités auditives. Il peut être utilisé comme matériel pédagogique (par les enseignants, les orthophonistes, les éducateurs spécialisés) ou comme matériel d''évaluation des capacités de reconnaissance auditive, sonore et vocale par les professionnels de l''audiophonologie.', NULL, 'img-aud_3.png', '375', 0, 10, NULL, 0, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=7 ;

--
-- Contenu de la table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created`, `modified`) VALUES
(1, 'clothing', '2013-11-03 15:18:13', '2013-11-03 15:18:13'),
(2, 'winter', '2013-11-03 15:18:33', '2013-11-03 15:18:33'),
(3, 'summer', '2013-11-03 15:18:36', '2013-11-03 15:18:36'),
(4, 'equipment', '2013-11-03 15:18:41', '2013-11-03 15:18:41'),
(5, 'black', '2013-11-03 15:39:35', '2013-11-03 15:39:35'),
(6, 'white', '2013-11-03 15:39:39', '2013-11-03 15:39:39');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cabinet` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profession` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8_unicode_ci,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pays` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `civilite` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=6 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `role`, `lastname`, `type`, `username`, `cabinet`, `profession`, `email`, `password`, `rue`, `ville`, `tel`, `cp`, `pays`, `active`, `created`, `modified`, `civilite`) VALUES
(1, 'admin', 'admin', '', 'admin', '', '', '', 'b9a51ed9f3d3e5cda1db52123001a1fe77955b93', NULL, '', '', '', '', 1, '2011-09-26 00:34:07', '2016-03-03 08:10:42', ''),
(2, 'customer', 'andras', '', 'andras', '', '', '', 'd043520c9576ccc8977ba16d6343375b61f9fab3', NULL, '', '', '', '', 1, '2013-10-29 16:58:16', '2013-10-30 01:56:38', ''),
(5, 'customer', 'user', '0', 'user', '', '', 'utilisateur@yahoo.com', '04f97bf9139451a0c12a9f0d27766695e4c547c9', '', '', '', '', '', 1, '2016-02-05 12:12:17', '2016-03-03 08:54:29', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
