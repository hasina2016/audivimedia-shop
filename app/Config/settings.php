<?php

$config = array(
    'Settings' => array(
        'WEBSITE' => 'http://localhost/',
        'SHOP_TITLE' => 'Audivimedia',
        'ADMIN_EMAIL' => '',
        'DOMAIN' => '',
        'ANALYTICS' => '',
        'PAYPAL_API_USERNAME' => '',
        'PAYPAL_API_PASSWORD' => '',
        'PAYPAL_API_SIGNATURE' => '',
        'AUTHORIZENET_ENABLED' => '1',
        'AUTHORIZENET_API_URL' => 'https://test.authorize.net/gateway/transact.dll',
        'AUTHORIZENET_API_LOGIN' => '',
        'AUTHORIZENET_API_TRANSACTION_KEY' => '',
        'TVA'=>0.2
        ),
    'Messages' => array(
        'LOG_ERROR'=> 'Combinaison Identifiant-Mot de passe incorrecte',
        'LOGOUT' => '',
        'ENTRETIEN'=>'Page Actuellement en maintenance'
        ),
    'Sogenactif' => array(
        'SUCCESS_MSG'=>'Bravo !Votre paiement a bien été pris en compte. Toute l’équipe d’Audivimedia vous remercie.',
        'ERROR_MSG'=>'Le paiment a échoué ou a été annulé :Audivimedia reste à votre entière disposition pour toute vos questions.',
        'ID'=>'014213245611111',
        'COUNTRY'=>'France',
        )
);


