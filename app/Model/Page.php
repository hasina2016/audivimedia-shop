<?php /**
* 
*/
App::uses('AppModel', 'Model');
class Page extends AppModel
{
	public $actsAs = array(
        'Tree'
    );
	public $hasMany = array(
        'ChildPage' => array(
            'className' => 'Page',
            'foreignKey' => 'parent_id',
            'dependent' => false
        ));
	public $belongsTo = array(
        'ParentPage' => array(
            'className' => 'Page',
            'foreignKey' => 'parent_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
} 
?>
