<?php /**
* 
*/
App::uses('AppModel', 'Model');
class Paragraphe extends AppModel
{
	public function getParagraphes($product_id = null)
	{
		if (!$product_id == null) {
			return $this->find('all',array(
			'conditions'=> array('Paragraphe.product_id'=>$product_id)
			));
		}
		return $this->find('all');
	}

	public $belongsTo = array('Product');
} 
?>