<?php 
App::uses('CakeEmail', 'Network/Email');
Class Contact extends AppModel{

	public $useTable = false;
	public $validate = array(
		'name' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Entrez votre nom'
			),
		'emai' => array(
			'rule' => 'email',
			'required' => true,
			'message' => 'Entrez un email valide'
			),

		'emai' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Veuillez ecrire un message'
			)
		);

	public function send($d){

		 $mail = new CakeEmail('smtp');
                $mail->to('rami.patrick301@gmail.com')
                     ->from($d['email'])
                     ->subject('Contact Utililisateur')
                     ->emailFormat('html')
                     ->template('contact')->viewVars($d);
                return $mail->send();

	}
}
?>