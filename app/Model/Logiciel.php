<?php
App::uses('AppModel', 'Model');
class Logiciel extends AppModel {

////////////////////////////////////////////////////////////

    public $validate = array(
        'name' => array(
            'rule1' => array(
                'rule' => array('between', 3, 60),
                'message' => 'Veiller specifier le nom',
                'allowEmpty' => false,
                'required' => false,
            ),
            'rule2' => array(
                'rule' => array('isUnique'),
                'message' => 'Nom dejà utilisé',
                'allowEmpty' => false,
                'required' => false,
            ),
        ),
        'price' => array(
            'notempty' => array(
                'rule' => array('decimal'),
                'message' => 'Prix invalide',
                //'allowEmpty' => false,
                'required' => true
                //'last' => fam_close(fam), // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

////////////////////////////////////////////////////////////

    public $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
        'Brand' => array(
            'className' => 'Brand',
            'foreignKey' => 'brand_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
    );

////////////////////////////////////////////////////////////

    public $hasMany = array(
        'Productmod',
        'Paragraphe',
        'Detail',
        'Page'
    );

////////////////////////////////////////////////////////////

    public function updateViews($products) {

        if(!isset($products[0])) {
            $a = $products;
            unset($products);
            $products[0] = $a;
        }

        $this->unbindModel(
            array('belongsTo' => array('Category', 'Brand'))
        );

        $productIds = Set::extract('/Product/id', $products);

        $this->updateAll(
            array(
                'Product.views' => 'Product.views + 1',
            ),
            array('Product.id' => $productIds)
        );


    }

////////////////////////////////////////////////////////////
    public function beforeSave($options = array())
    {
        $this->data['Logiciel']['description_fr'] = utf8_encode($this->data['Logiciel']['description_fr']);
        $this->data['Logiciel']['description_en'] = utf8_encode($this->data['Logiciel']['description_en']);
        $this->data['Logiciel']['description_es'] = utf8_encode($this->data['Logiciel']['description_es']);
        return true;
    }
////////////////////////////////////////////////////////////

}