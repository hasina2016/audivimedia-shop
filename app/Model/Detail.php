<?php /**
* 
*/
App::uses('AppModel', 'Model');
class Detail extends AppModel
{
	public $belongsTo = array(
			'Product'=>array(
				'className'=>'Product',
				'foreingKey'=>'product_id',
				'depentend'=> false
				),
			'Category'=>array(
				'className'=>'Category',
				'foreingKey'=>'category_id',
				'depentend'=> false
				)
		);
	public $hasMany = array(
		'Category'
		);
/////////////////////////////////////////////////////////////////////
	public function getCategories($category_id = null)
	{
		if ($category_id == null) {
			return $this->Category->find('all',array(
				'conditions'=>array('Category.name NOT'=>'main')
			));
		}
		return $this->Category->find('all',array(
			'conditions'=>array( 
				'Category.id'=>$category_id,
				'Category.name NOT'=>'main'
				)
			));
	}
} ?>