<?php /**
* 
*/
class DetailController extends AppController
{
	public function getCategories($pruduct_id)
	{
		return $this->find('all', array(
			'condition'=>array('Detail.logiciel_id'=>$pruduct_id)
			));
	}
	public $belongsTo = array(
		'Logiciel'=> array(
			'className'=>'Logiciel',
			'foreignKey'=>'logiciel_id',
			'dependent' => false
			),
		'Category'
		);
} ?>