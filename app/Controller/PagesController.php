<?php /**
* 
*/
App::uses('AppController', 'Controller');
App::uses('Xml', 'Utility');
class PagesController extends AppController
{
    public $helpers = array('Js');
    public function mentions()
    {
        //mentions legales
    }
    public function cgv()
    {
        //condition générale de vente
    }
    public function contacte()
    {
        //contacte
    }
    public function commande()
    {
        //commandes
    }
    public function read()
    {
        //commandes
    }
    public function equipe()
    {
        //commandes
    }
    public function trouble()
    {
        //commandes
    }

    public function admin_add()
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        if ($this->request->is('post')) {
            
            $logid = $this->request->query('logid');
            $prodid = $this->request->query('prodid');
            
            if (!isset($logid)) {$logid = 0;};         
            if (!isset($prodid)) {$prodid = 0;};

            $datap = array(
            'title'=>  $this->request->data['Page']['title'],
            'autor'=>  $this->request->data['Page']['autor'],
            'body_fr'=>$this->request->data['Page']['body_fr'],
            'body_en'=>$this->request->data['Page']['body_en'],
            'body_es'=>$this->request->data['Page']['body_es'],
            'parent_id'=>  $this->request->data['Page']['parent_id'],
            'logiciel_id'=>$logid,
            'product_id'=>$prodid
            );
            $this->Page->create();
            if ($this->Page->save($datap)) {
                $this->Session->setFlash('Contenue Sauvegardé   ');
                return $this->redirect(array('controller'=>'users','action'=>'dashboard'));
            } else {
                $this->Session->setFlash('Enregistrement echoué.');
            }
        }

        $parents = $this->Page->generateTreeList(null, null, null, ' -- ');
        $d = $this->request->query['amp;item'];
        $item = $this->request->query('amp;item'); //tsy azoko le amp;
        $this->set(compact('parents','item','id'));
    }
    public function admin_index()
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $contents = $this->Page->find('all');
        $this->set(compact('contents'));
    }
    public function admin_view($id )            
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $this->layout = 'empty';
        $page = $this->Page->find('first',array(
            'conditions'=>array('Page.id'=>$id)
            ));
        $this->set(compact('page'));
    }
/////////////////////////////////////////////////////////////////////////////////////
    public function admin_acceuil()
    {
        $xml = Xml::build('conf/pages.xml',array('return' => 'domdocument'));
        if ($this->request->is('Ajax')) {
            $this->layout = 'empty';
        }
        $this->loadModel('Logiciel');
        $familles = $this->Logiciel->find('all', array(
            'conditions'=>array(
                'Logiciel.prompted'=>true,
                'OR'=>array(
                        array('Logiciel.id'=>$xml->getElementsByTagName('family_1')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('family_2')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('family_3')->item(0)->nodeValue),
                    )
                )
            ));

        $pros = $this->Logiciel->find('all', array(
            'conditions'=>array(
                'Logiciel.prompted'=>true,
                'OR'=>array(
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_1')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_2')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_3')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_4')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_5')->item(0)->nodeValue),
                    )
                )
            ));        
        $news = $this->Logiciel->find('all', array(
            'conditions'=>array(
                'Logiciel.prompted'=>true,
                'OR'=>array(
                        array('Logiciel.id'=>$xml->getElementsByTagName('new_1')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('new_2')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('new_3')->item(0)->nodeValue),
                    )
                )
            ));
        $this->loadModel('Category');
        $categories = $this->Category->find('all',array(
            'fields'=>'Category.name',
            'conditions'=>array('Category.parent_id'=>3)
            ));
        $this->set(compact('familles','pros','news','categories','xml'));
    }
/////////////////////////////////////////////////////////////////////////////////////
    public function admin_config()
    {
        $this->autoRender = false;
        $id = $this->request->data['id'];
        $xml = Xml::build('conf/pages.xml',array('return' => 'domdocument'));
        if (!$this->request->data['fr']=='') {
            $xml->getElementsByTagName($id.'_fr')->item(0)->nodeValue = $this->request->data['fr'];
        }if (!$this->request->data['en']=='') {
            $xml->getElementsByTagName($id.'_en')->item(0)->nodeValue = $this->request->data['en'];
        }if (!$this->request->data['es']=='') {
            $xml->getElementsByTagName($id.'_es')->item(0)->nodeValue = $this->request->data['es'];
        }
        $xml->save('conf/pages.xml');
        return true;
    }
/////////////////////////////////////////////////////////////////////////////////////
    public function admin_bottomTitle()
    {
        $this->autoRender = false;
        $xml = Xml::build('conf/pages.xml',array('return' => 'domdocument'));
        if (!$this->request->data['title_fr']=='') {
            $xml->getElementsByTagName('title_fr')->item(0)->nodeValue = $this->request->data['title_fr'];
        }if (!$this->request->data['title_en']=='') {
            $xml->getElementsByTagName('title_en')->item(0)->nodeValue = $this->request->data['title_en'];
        }if (!$this->request->data['title_es']=='') {
            $xml->getElementsByTagName('title_es')->item(0)->nodeValue = $this->request->data['title_es'];
        }
        $xml->save('conf/pages.xml');
        return true;
    }
/////////////////////////////////////////////////////////////////////////////////////
    public function admin_bottomCnt()
    {
        $this->autoRender = false;
        $xml = Xml::build('conf/pages.xml',array('return' => 'domdocument'));
        if (!$this->request->data['cnt_fr']=='') {
            $xml->getElementsByTagName('cnt_fr')->item(0)->nodeValue = $this->request->data['cnt_fr'];
        }if (!$this->request->data['cnt_en']=='') {
            $xml->getElementsByTagName('cnt_en')->item(0)->nodeValue = $this->request->data['cnt_en'];
        }if (!$this->request->data['cnt_es']=='') {
            $xml->getElementsByTagName('cnt_es')->item(0)->nodeValue = $this->request->data['cnt_es'];
        }
        $xml->save('conf/pages.xml');
        return true;
    }
/////////////////////////////////////////////////////////////////////////////////////
    public function admin_edit($id)
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        if (!$this->Page->exists($id)) {
            throw new NotFoundException('Contenue Invalide');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Page->id = $id;
            if ($this->Page->save($this->request->data)) {
                $this->Session->setFlash('Sauvegarde éffectué');
                return $this->redirect(array('controller'=>'users','action'=>'dashboard'));
            } else {
                $this->Session->setFlash('Impossible d\'éffectué le sauvegarde');
            }
        } else {
            $page = $this->Page->find('first', array(
                'conditions' => array(
                    'Page.id' => $id
                )
            ));
            $this->request->data = $page;
        }

        $parents = $this->Page->generateTreeList(null, null, null, ' -- ');
        $this->set(compact('page','parents'));
    }
    public function admin_edittoogle($id)
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
            $this->autoRender = false;
        }
        $this->Page->id = $id;
        $this->Page->saveField($this->request->data['name'],$this->request->data['value']);
    }
} ?>