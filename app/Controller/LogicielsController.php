<?php
App::uses('AppController', 'Controller');
App::uses('Xml', 'Utility');
class LogicielsController extends AppController {

////////////////////////////////////////////////////////////

    public $components = array(
        'RequestHandler'
    );
    public $helpers = array('Js');
////////////////////////////////////////////////////////////

    public function beforeFilter() {
        parent::beforeFilter();
        $menu = array(
        'main-menu' => array(
            array(
                'title' => 'Home',
                'url' => array('controller' => 'logiciels', 'action' => 'index'),
                'class' => array('test', 'red')
            ),
            array(
                'title' => 'About Us',
                'url' => '#'
            ),
        )
    );

    // For default settings name must be menu
    $this->set(compact('menu'));
    }

////////////////////////////////////////////////////////////

/*    public function index() {
        $logiciels = $this->Logiciel->find('all', array(
            'recursive' => -1,
            'contain' => array(
                'Brand'
            ),
            'limit' => 20,
            'conditions' => array(
                'logiciel.active' => 1,
                'Brand.active' => 1
            ),
            'order' => array(
                'logiciel.views' => 'ASC'
            )
        ));
        $this->set(compact('logiciels'));

        $this->Logiciel->updateViews($logiciels);

        $this->set('title_for_layout', Configure::read('Settings.SHOP_TITLE'));
    }
*/
    public function index()
    {
        $xml = Xml::build('conf/pages.xml',array('return' => 'domdocument'));
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $familles = $this->Logiciel->find('all', array(
            'conditions'=>array(
                'Logiciel.prompted'=>true,
                'OR'=>array(
                        array('Logiciel.id'=>$xml->getElementsByTagName('family_1')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('family_2')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('family_3')->item(0)->nodeValue),
                    )
                )
            ));

        $pros = $this->Logiciel->find('all', array(
            'conditions'=>array(
                'Logiciel.prompted'=>true,
                'OR'=>array(
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_1')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_2')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_3')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_4')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('pro_5')->item(0)->nodeValue),
                    )
                )
            ));        
        $news = $this->Logiciel->find('all', array(
            'conditions'=>array(
                'Logiciel.prompted'=>true,
                'OR'=>array(
                        array('Logiciel.id'=>$xml->getElementsByTagName('new_1')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('new_2')->item(0)->nodeValue),
                        array('Logiciel.id'=>$xml->getElementsByTagName('new_3')->item(0)->nodeValue),
                    )
                )
            ));

        $all = $this->Logiciel->find('list',array(
             'fields'=>array('Logiciel.name_fr'),
            ));
        $this->loadModel('Category');
        $categories = $this->Category->find('all',array(
            'fields'=>'Category.name',
            'conditions'=>array('Category.parent_id'=>3)
            ));
        $this->set(compact('familles','pros','categories','news','xml','all'));
        $this->set('title_for_layout', Configure::read('Settings.SHOP_TITLE'));
    }
////////////////////////////////////////////////////////////

    public function famille() { 
        $logiciels = $this->Logiciel->find('all',array(
            'contain' => 'Detail',
            'conditions'=> array('Logiciel.active'=>true)
            ));
        $this->set(compact('logiciels'));
        $this->set('title_for_layout', 'Logiciel Famille ' .'| '. Configure::read('Settings.SHOP_TITLE'));
    }
/////////////////////////////////////////////////////////////////

    public function audioprothesistes() { 
        $logiciels = $this->Logiciel->find('all',array(
            'contain' => 'Detail',
            'conditions'=> array('Logiciel.active'=>true)
            ));
        $this->set(compact('logiciels'));
        $this->set('title_for_layout', 'audioprothesistes ' .'| '. Configure::read('Settings.SHOP_TITLE'));
    }


////////////////////////////////////////////////////////////
    public function orlphoniatres() { 
        $logiciels = $this->Logiciel->find('all',array(
            'contain' => 'Detail',
            'conditions'=> array('Logiciel.active'=>true)
            ));
        $this->set(compact('logiciels'));
        $this->set('title_for_layout', 'orl & phoniatres ' .'| '. Configure::read('Settings.SHOP_TITLE'));

    }
///////////////////////////////////////////////////////////

    public function orthophonistes() { 
        $logiciels = $this->Logiciel->find('all',array(
            'contain' => 'Detail',
            'conditions'=> array('Logiciel.active'=>true)
            ));
        $this->set(compact('logiciels'));
        $this->set('title_for_layout', 'orl & phoniatres ' .'| '. Configure::read('Settings.SHOP_TITLE'));

    }
///////////////////////////////////////////////////////////   
    
    public function bycategory() { 

        $this->Paginator = $this->Components->load('Paginator');

        $this->loadModel('Category');
        $categories = $this->Category->find('all', array(
                'conditions' => array('Category.parent_id'=> 3)
            ));
        $logiciels = $this->Logiciel->find('all',array(
            'contain' => 'Detail',
            'conditions'=> array('Logiciel.active'=>true)
            ));
        $this->set(compact('logiciels'));
        $this->set(compact('categories'));
        $this->set('title_for_layout', 'Logiciel adapté à votre métier ' .'| '. Configure::read('Settings.SHOP_TITLE'));

    }
///////////////////////////////////////////////////////////   

    public function view($id = null) {
        if ($this->request->is('ajax')) {
            $this->layout = 'popup';
        }
        $logiciel = $this->Logiciel->find('first', array(
                'contain'=>array('Page'=>array(
                        'conditions'=>array('Page.active'=>true)
                    )),
                'conditions'=>array('Logiciel.id' => $id)
            ) );
        $this->set(compact('logiciel'));
        $this->set('title_for_layout', $logiciel['Logiciel']['name_fr'] . '| ' . Configure::read('Settings.SHOP_TITLE'));

    }

////////////////////////////////////////////////////////////

    public function search() {

        $search = null;
        if(!empty($this->request->query['search']) || !empty($this->request->data['name'])) {
            $search = empty($this->request->query['search']) ? $this->request->data['name'] : $this->request->query['search'];
            $search = preg_replace('/[^a-zA-Z0-9 ]/', '', $search);
            $terms = explode(' ', trim($search));
            $terms = array_diff($terms, array(''));
            $conditions = array(
                'Brand.active' => 1,
                'logiciel.active' => 1,
            );
            foreach($terms as $term) {
                $terms1[] = preg_replace('/[^a-zA-Z0-9]/', '', $term);
                $conditions[] = array('logiciel.name LIKE' => '%' . $term . '%');
            }
            $logiciels = $this->Logiciel->find('all', array(
                'recursive' => -1,
                'contain' => array(
                    'Brand'
                ),
                'conditions' => $conditions,
                'limit' => 200,
            ));
            if(count($logiciels) == 1) {
                return $this->redirect(array('controller' => 'logiciels', 'action' => 'view', 'slug' => $logiciels[0]['logiciel']['slug']));
            }
            $terms1 = array_diff($terms1, array(''));
            $this->set(compact('logiciels', 'terms1'));
        }
        $this->set(compact('search'));

        if ($this->request->is('ajax')) {
            $this->layout = false;
            $this->set('ajax', 1);
        } else {
            $this->set('ajax', 0);
        }

        $this->set('title_for_layout', 'Search');

        $description = 'Search';
        $this->set(compact('description'));

        $keywords = 'search';
        $this->set(compact('keywords'));
    }

////////////////////////////////////////////////////////////

    public function searchjson() {

        $term = null;
        if(!empty($this->request->query['term'])) {
            $term = $this->request->query['term'];
            $terms = explode(' ', trim($term));
            $terms = array_diff($terms, array(''));
            $conditions = array(
                // 'Brand.active' => 1,
                'logiciel.active' => 1
            );
            foreach($terms as $term) {
                $conditions[] = array('logiciel.name LIKE' => '%' . $term . '%');
            }
            $logiciels = $this->Logiciel->find('all', array(
                'recursive' => -1,
                'contain' => array(
                    // 'Brand'
                ),
                'fields' => array(
                    'logiciel.id',
                    'logiciel.name',
                    'logiciel.image'
                ),
                'conditions' => $conditions,
                'limit' => 20,
            ));
        }
        // $logiciels = Hash::extract($logiciels, '{n}.logiciel.name');
        echo json_encode($logiciels);
        $this->autoRender = false;

    }

////////////////////////////////////////////////////////////

    public function sitemap() {
        $logiciels = $this->Logiciel->find('all', array(
            'recursive' => -1,
            'contain' => array(
                'Brand'
            ),
            'fields' => array(
                'logiciel.slug'
            ),
            'conditions' => array(
                'Brand.active' => 1,
                'logiciel.active' => 1
            ),
            'order' => array(
                'logiciel.created' => 'DESC'
            ),
        ));
        $this->set(compact('logiciels'));

        $website = Configure::read('Settings.WEBSITE');
        $this->set(compact('website'));

        $this->layout = 'xml';
        $this->response->type('xml');
    }

////////////////////////////////////////////////////////////

    public function admin_reset() {
        $this->Session->delete('logiciel');
        return $this->redirect(array('action' => 'index'));
    }

////////////////////////////////////////////////////////////
    public function admin_prompted()
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
            $this->autoRender = false;
        }
        $logiciels = $this->Logiciel->find('list',array(
            'fields'=>array('Logiciel.name_fr'),
            'conditions'=>array(
                'AND'=>array(
                    'Logiciel.active'=> true,
                    'Logiciel.prompted'=> true,
                    )
                ),
            ));
        return $logiciels = json_encode($logiciels);
        //$this->set(compact('logiciels'));
    }
////////////////////////////////////////////////////////////
    public function admin_index()
    {
        $this->loadModel('Category');
        $logiciels = $this->Logiciel->find('all',array(
            'contain'=>'Detail.category_id'
            ));
        $categories = $this->Category->find('all',array(
            'contain'=>'Detail.logiciel_id',
            'conditions'=>array(
                'NOT'=>array('Category.id'=>0)
                )
            
            ));
        $this->set(compact('logiciels','categories'));
    }
////////////////////////////////////////////////////////////

    public function _upload($id = null)
    {
        $logiciel = $this->Logiciel->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'logiciel.id' => $id
            )
        ));
        $this->set(compact('logiciel'));

        if (($this->request->is('post') || $this->request->is('put'))) {

            $this->Img = $this->Components->load('Img');

            $newName = $this->request->data['logiciel']['id'];

            $ext = $this->Img->ext($this->request->data['logiciel']['image']['name']);

            $origFile = $newName . '.' . $ext;
            $dst = $newName . '.jpg';

            $targetdir = WWW_ROOT . 'images/original';

            $upload = $this->Img->upload($this->request->data['logiciel']['image']['tmp_name'], $targetdir, $origFile);

            if($upload == 'Success') {
                $this->Img->resampleGD($targetdir . DS . $origFile, WWW_ROOT . 'images/large/', $dst, 800, 800, 1, 0);
                $this->Img->resampleGD($targetdir . DS . $origFile, WWW_ROOT . 'images/small/', $dst, 180, 180, 1, 0);
                $this->request->data['logiciel']['image'] = $dst;
            } else {
                $this->request->data['logiciel']['image'] = '';
            }
        }
        $this->set('data',$this->request->data);
    }

////////////////////////////////////////////////////////////

    public function admin_view($id = null) {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $log = $this->Logiciel->find('first', array(
            'conditions'=>array('logiciel.id'=>$id),
            'contain'=> array('Page')
            ));
        $this->set(compact('log'));
    }

////////////////////////////////////////////////////////////

    public function admin_add() {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        $this->loadModel('Paragraphe');
        if ($this->request->is('post')) {
        $dprod = array(
            'name'=> $this->request->data['Logiciel']['name'],
            'name_en'=> $this->request->data['Logiciel']['name_en'],
            'name_es'=> $this->request->data['Logiciel']['name_es'],
            'age'=>$this->request->data['Logiciel']['age'],
            'category_id'=>$this->request->data['Logiciel']['category_id'],
            'win'=>$this->request->data['Logiciel']['win'],
            'mac'=>$this->request->data['Logiciel']['mac'],
            'telechargement'=>$this->request->data['Logiciel']['telechargement'],
            'cdrom'=>$this->request->data['Logiciel']['cdrom'],
            'usb'=>$this->request->data['Logiciel']['usb'],
            'price'=>$this->request->data['Logiciel']['price'],
            'image_max'=>$this->request->data['Logiciel']['image']['name'],
            );
/////////////////////////////////////////////////////////// UPLOAD IMAGE
            $this->Img = $this->Components->load('Img');

            $newName = $this->request->data['Logiciel']['image']['name'];

            $ext = $this->Img->ext($this->request->data['Logiciel']['image']['name']);

            //$this->Media->save(array('name'=>$newName));

            $origFile = $newName;// . '.' . $ext;
            $dst = $newName;

            $targetdir = WWW_ROOT . 'images/original';

            $upload = $this->Img->upload($this->request->data['Logiciel']['image']['tmp_name'], $targetdir, $origFile);

            if($upload == 'Success') {
                $this->Img->resampleGD($targetdir . DS . $origFile, WWW_ROOT . 'images/large/', $dst, 800, 800, 1, 0);
                $this->Img->resampleGD($targetdir . DS . $origFile, WWW_ROOT . 'images/small/', $dst, 180, 180, 1, 0);
                $this->request->data['Logiciel']['image'] = $dst;
            } else {
                $this->request->data['Logiciel']['image'] = '';
            }
//////////////////////////////////////////////////////////
            $this->Logiciel->create();
            if ($this->Logiciel->save($dprod)) {
                $this->Session->setFlash('Sauvegarde terminé');
                return $this->redirect(array('controller'=>'users','action'=>'dashboard'));
            } else {
                $this->Session->setFlash('Impossible d\'éffectué le sauvegarde');
            }
        }
        $this->set('data',$this->request->data);
        //$brands = $this->Logiciel->Brand->find('list');
        //$this->set(compact('brands'));
        $categories = $this->Logiciel->Category->generateTreeList(null, null, null, '--');
        $this->set(compact('categories'));
    }

////////////////////////////////////////////////////////////

    public function admin_edit($id = null) {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
        }
        if (!$this->Logiciel->exists($id)) {
            throw new NotFoundException('Invalid logiciel');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            //$this->Logiciel->id=$id;
            if ($this->Logiciel->save($this->request->data)) {
                $this->Session->setFlash('Sauvegarde éffectué');
                return $this->redirect(array('controller'=>'users','action'=>'dashboard'));
            } else {
                $this->Session->setFlash('Impossible d\'éffectué le sauvegarde');
            }
        } else {
            $logiciel = $this->Logiciel->find('first', array(
                'conditions' => array(
                    'logiciel.id' => $id
                )
            ));
            $this->request->data = $logiciel;
        }

        $this->set(compact('logiciel'));

        $categories = $this->Logiciel->Category->generateTreeList(null, null, null, '--');
        $this->set(compact('categories'));
    }

////////////////////////////////////////////////////////////
    public function admin_edittoogle()
    {
        if ($this->request->is('ajax')) {
            $this->layout = 'empty';
            $this->autoRender = false;
        }
        $data_ref = $this->Logiciel->find('first',array(
            'fields'=>array(
                'Logiciel.id',
                'Logiciel.description_fr',
                'Logiciel.image_max'
                ),
            'conditions'=>array('Logiciel.id'=>(int)$this->request->data['value'])
        ));
        $ref = array( 
            'image'=>utf8_encode($data_ref['Logiciel']['image_max']),
            'description'=>utf8_encode($data_ref['Logiciel']['description_fr']),
            'id'=>utf8_encode($this->request->data['pk'])
            );

        $xml = Xml::build('conf/pages.xml',array('return' => 'domdocument'));
        $xml->getElementsByTagName($this->request->data['dataOrg'])->item(0)->nodeValue = $this->request->data['value'];
        $xml->save('conf/pages.xml');
        $result = json_encode($ref,JSON_PRETTY_PRINT);
        return $result;
    }
////////////////////////////////////////////////////////////
    public function admin_switch($field=null, $id=null)
    {
        $this->autoRender=false;
        
    }
////////////////////////////////////////////////////////////

    public function admin_tags($id = null) {

        $tags = ClassRegistry::init('Tag')->find('all', array(
            'recursive' => -1,
            'fields' => array(
                'Tag.name'
            ),
            'order' => array(
                'Tag.name' => 'ASC'
            ),
        ));
        $tags = Hash::combine($tags, '{n}.Tag.name', '{n}.Tag.name');
        $this->set(compact('tags'));

        if ($this->request->is('post') || $this->request->is('put')) {

            $tagstring = '';

            foreach($this->request->data['logiciel']['tags'] as $tag) {
                $tagstring .= $tag . ', ';
            }

            $tagstring = trim($tagstring);
            $tagstring = rtrim($tagstring, ',');

            $this->request->data['logiciel']['tags'] = $tagstring;

            $this->Logiciel->save($this->request->data, false);

            return $this->redirect(array('action' => 'tags', $this->request->data['logiciel']['id']));

        }

        $logiciel = $this->Logiciel->find('first', array(
            'conditions' => array(
                'logiciel.id' => $id
            )
        ));
        if (empty($logiciel)) {
            throw new NotFoundException('Invalid logiciel');
        }
        $this->set(compact('logiciel'));

        $selectedTags = explode(',', $logiciel['logiciel']['tags']);
        $selectedTags = array_map('trim', $selectedTags);
        $this->set(compact('selectedTags'));

        $neighbors = $this->Logiciel->find('neighbors', array('field' => 'id', 'value' => $id));
        $this->set(compact('neighbors'));

    }

////////////////////////////////////////////////////////////

    public function admin_csv() {
        $logiciels = $this->Logiciel->find('all', array(
            'recursive' => -1,
        ));
        $this->set(compact('logiciels'));
        $this->layout = false;
    }

////////////////////////////////////////////////////////////

    public function admin_delete($id = null) {
        $this->Logiciel->id = $id;
        if (!$this->Logiciel->exists()) {
            throw new NotFoundException('Invalid logiciel');
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Logiciel->delete()) {
            $this->Session->setFlash('Logiciel supprimé');
            return $this->redirect(array('controller'=>'users','action'=>'dashboard'));
        }
        $this->Session->setFlash('Erreur de suppression');
        return $this->redirect(array('action' => 'index'));
    }

////////////////////////////////////////////////////////////

    // Hasina's implementations
    //Prompted logiciel
    public function _getPrompted()
    {
        $logiciels = $this->Logiciel->find('all', array(
            'condition'=> array('logiciel.prompted' => 1)
            ));
        $this->set(compact('logiciels'));
    }

///////////////////////////////////////////////////////////

}
