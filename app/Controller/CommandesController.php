<?php /**
* 
*/
class CommandesController extends AppController
{
	
	public function index()
	{
		$this->loadModel('Logiciel');
		$this->loadModel('Category');
		$items = $this->Logiciel->find('all',array(
			'conditions'=>array('Logiciel.active'=>true),
			'contain'=>array('Detail')
			));
		$categories = $this->Category->find('list',array(
			'fields'=>array('Category.name')
			));
		$this->set(compact('items','categories'));
	}
} ?>